run("3D Manager V4 Macros"); // import 3DManager macro extension
Ext.Manager3DV4_ImportImage(); // import current label image
c print(class); // get the class of objet 0
Ext.Manager3DV4_SetClass(0, 1); // set the class of object 0
Ext.Manager3DV4_GetClass(0, class); print(class); // get the type of objet 0
Ext.Manager3DV4_MeasureList(); // print the list of available measurements
Ext.Manager3DV4_Measure(0, "Volume(Unit)", mes); print(mes); // performs measurement on object 0
Ext.Manager3DV4_MeasureIntensity(0, "CMX(unit)", mes); print(mes); // performs intensity measurement on object 0
Ext.Manager3DV4_DistanceList(); // List available distance  between two objects
Ext.Manager3DV4_Distance2(0, 1, "DistCenterCenterPix", dist); print(dist); // distance between first and second object
Ext.Manager3DV4_NbObjects(nb); print(nb); // get the total number of objects
newImage("draw", "RGB black", getWidth(), getHeight(), nSlices); // new image to fill objects with some colour
for(i=0;i<nb;i++){ // loop for all object indices
    red = 128 + random*127;
    green = 128 + random*127;
    blue = 128 + random*127;
    Ext.Manager3DV4_3DViewer(i, red, green, blue); // displays object i in 3D viewer with specified colour
    Ext.Manager3DV4_3DFill(i, red, green, blue); // fill objects with specified colour
}
    

