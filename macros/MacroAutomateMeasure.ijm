input_path = getDirectory("input files");
fileList = getFileList(input_path);
run("Close All");
setBatchMode("hide");
for (f=0; f<fileList.length; f++){
	if (endsWith(fileList[f], ".zip")) {
		open(input_path + fileList[f]);
		print(input_path + fileList[f]);
		run("3D Geometrical Measure");
		close();
	}
}
setBatchMode("show");