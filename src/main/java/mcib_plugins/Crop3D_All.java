package mcib_plugins;

import ij.IJ;
import ij.ImagePlus;
import ij.Prefs;
import ij.gui.GenericDialog;
import ij.plugin.filter.PlugInFilter;
import ij.process.ImageProcessor;
import mcib3d.geom2.Objects3DIntPopulation;
import mcib3d.image3d.ImageHandler;
import mcib3d.image3d.processing.ImageCropper;

import java.io.File;
import java.util.List;

/**
 * plugin de crop 3D
 *
 * @author Thomas BOUDIER @created juin 2007
 */
public class Crop3D_All implements PlugInFilter {

    ImagePlus imp;
    String dir = Prefs.get("Crop3DAll_dir.string", "Existing Folder");

    /**
     * Main processing method for the Crop3D_ object
     *
     * @param ip Description of the Parameter
     */
    @Override
    public void run(ImageProcessor ip) {
        if (Dialogue()) {
            // test folder
            File dirFile = new File(dir);
            if (!dirFile.exists()) {
                IJ.log("Directory " + dir + " does not exist, please create it before running this plugin.");
                return;
            }
            Prefs.set("Crop3DAll_dir.string", dir);
            ImageHandler ima = ImageHandler.wrap(imp);
            Objects3DIntPopulation population = new Objects3DIntPopulation(ima);
            List<ImageHandler> crops = new ImageCropper(ima).cropBoundingBoxMaskPopulation(population);
            String name = ima.getTitle();
            for (ImageHandler crop : crops) {
                crop.setTitle(name + "_" + crop.getTitle() + ".tif");
                crop.save(dir);
            }
        }
    }

    /**
     * Description of the Method
     *
     * @return Description of the Return Value
     */
    private boolean Dialogue() {
        GenericDialog gd = new GenericDialog("3D Crop All");
        gd.addDirectoryField("Choose directory", IJ.getDirectory("home"));
        gd.showDialog();

        dir = gd.getNextString();

        return (!gd.wasCanceled());
    }

    /**
     * Description of the Method
     *
     * @param arg Description of the Parameter
     * @param imp Description of the Parameter
     * @return Description of the Return Value
     */
    @Override
    public int setup(String arg, ImagePlus imp) {
        this.imp = imp;
        return DOES_8G + DOES_16 + DOES_32;
    }
}
