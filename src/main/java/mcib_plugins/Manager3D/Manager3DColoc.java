package mcib_plugins.Manager3D;

import mcib3d.geom2.Object3DInt;
import mcib3d.geom2.Objects3DIntPopulation;
import mcib3d.geom2.measurements.Measure2Colocalisation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public class Manager3DColoc {
    public static ResultsFrame coloc3D(List<Object3DInt> object3DListA, List<Object3DInt> object3DListB) {
        if ((object3DListA == null) || (object3DListB == null)) return null;

        Objects3DIntPopulation populationA = new Objects3DIntPopulation();
        object3DListA.forEach(populationA::addObject);
        Objects3DIntPopulation populationB = new Objects3DIntPopulation();
        object3DListB.forEach(populationB::addObject);

        List<String> headings = new ArrayList<>();
        headings.add("Nb");
        headings.add("Name");
        headings.add("Label");
        headings.add("Type");
        headings.add("O1"); // object label coloc 1
        headings.add("N1"); // object name coloc 1
        headings.add("T1"); // object type coloc 1
        headings.add("V1"); // volume coloc 1
        headings.add("P1"); // % coloc 1

        Map<Object3DInt, List<Object>> map = new HashMap<>();
        AtomicInteger ai = new AtomicInteger(0);
        AtomicInteger maxObj = new AtomicInteger(0);
        object3DListA.forEach(objA -> {
            List<Object> list = new ArrayList<>();
            list.add(ai.get() + 1);
            list.add(objA.getName());
            list.add(objA.getLabel());
            list.add(objA.getType());
            list.add(0); // label
            list.add("NA"); // name
            list.add(0); // type
            list.add(0); // volume
            list.add(0); // pc
            String[] colocs = getColoc(objA, object3DListB);
            //
            if (colocs.length > 0) {
                list.set(4, Float.parseFloat(colocs[0])); // label
                list.set(5, colocs[1]); // name
                list.set(6, Integer.parseInt(colocs[2])); // type
                list.set(7, Double.parseDouble(colocs[3])); // volume
                list.set(8, 100.0 * Double.parseDouble(colocs[3]) / objA.size()); // pc
                if (colocs.length > 4) {
                    maxObj.set(Math.max(colocs.length / 4, maxObj.get()));
                    for (int i = 4; i < colocs.length; i += 4) {
                        list.add(Float.parseFloat(colocs[i])); // label
                        list.add(colocs[i]); // name
                        list.add(Integer.parseInt(colocs[2])); // type
                        list.add(Double.parseDouble(colocs[3])); // volume
                        list.add(100.0 * Double.parseDouble(colocs[3]) / objA.size()); // pc
                    }
                }
            }
            map.put(objA, list);
            ai.incrementAndGet();
        });

        for (int i = 2; i <= maxObj.get(); i++) {
            headings.add("O" + i); // object label coloc
            headings.add("N" + i); // object name coloc
            headings.add("T" + i); // object type coloc
            headings.add("V" + i); // volume coloc
            headings.add("P" + i); // % coloc
        }

        // create data
        Object[][] data = new Object[object3DListA.size()][headings.size()];
        for (int i = 0; i < object3DListA.size(); i++) {
            Object3DInt objA = object3DListA.get(i);
            for (int j = 0; j < headings.size(); j++) {
                Object val = 0;
                if (j < map.get(objA).size()) val = map.get(objA).get(j);
                data[i][j] = val;
            }
        }

        ResultsFrame tableResultsMeasure = new ResultsFrame("3D Coloc", headings.toArray(new String[0]), data, ResultsFrame.OBJECT_1);
        tableResultsMeasure.setObjectsList(object3DListA);

        return tableResultsMeasure;
    }


    private static String[] getColoc(Object3DInt obj, List<Object3DInt> object3DList) {
        List<String> values = new ArrayList<>();

        object3DList.forEach(obj2 -> {
            Measure2Colocalisation colocalisation = new Measure2Colocalisation(obj, obj2);
            double volume = colocalisation.getValue(Measure2Colocalisation.COLOC_VOLUME);
            double pc = colocalisation.getValue(Measure2Colocalisation.COLOC_PC);

            if ((volume > 0) && (obj != obj2)) {
                values.add(String.valueOf((double) obj2.getLabel())); // label 2
                values.add(obj2.getName()); // name 2
                values.add(String.valueOf(obj2.getType())); // type 2
                values.add(String.valueOf(volume)); // volume 2
            }
        });

        return values.toArray(new String[0]);
    }
}
