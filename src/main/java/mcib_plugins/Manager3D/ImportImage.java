package mcib_plugins.Manager3D;

import ij.Prefs;
import ij.gui.Roi;
import mcib3d.geom.Point3D;
import mcib3d.geom2.Object3DInt;
import mcib3d.geom2.Objects3DIntPopulation;
import mcib3d.geom2.Objects3DIntPopulationComputation;
import mcib3d.geom2.measurements.MeasureCentroid;
import mcib3d.image3d.ImageHandler;

import java.util.List;
import java.util.stream.Collectors;

public class ImportImage {
    ImageHandler image;

    public ImportImage(ImageHandler image) {
        this.image = image;
    }

    public Objects3DIntPopulation importImage() {
        Objects3DIntPopulation population = new Objects3DIntPopulation(image);
        // options
        final double minSize = Prefs.get("RoiManager3D-V3-Options_MinSegment.double", 0);
        double maxSize = Prefs.get("RoiManager3D-V3-Options_MaxSegment.double", Double.MAX_VALUE);
        if (maxSize == -1) maxSize = Double.MAX_VALUE;
        final double maxSize1 = maxSize;
        // check borders
        boolean touchXY = Prefs.get("RoiManager3D-V3-Options_excludeXY.boolean", false);
        boolean touchZ = Prefs.get("RoiManager3D-V3-Options_excludeZ.boolean", false);
        // FIXME check in XY
        if (touchXY || touchZ)
            population = new Objects3DIntPopulationComputation(population).getExcludeBorders(image, touchZ);
        // size
        if ((minSize > 0) || (maxSize < Double.MAX_VALUE)) {
            population = new Objects3DIntPopulationComputation(population).getFilterSize(minSize, maxSize);
        }
        // roi
        if (Prefs.get("RoiManager3D-V3-Options_useRoi.boolean", false)) {
            Roi roi = image.getImagePlus().getRoi();
            if (roi != null) {
                System.out.println("Import using roi " + roi);
                List<Object3DInt> toKeep = population.getObjects3DInt().stream().filter(obj -> {
                    Point3D centroid = new MeasureCentroid(obj).getCentroidAsPoint();
                    return (roi.contains(centroid.getRoundX(), centroid.getRoundY()));
                }).collect(Collectors.toList());
                population = new Objects3DIntPopulation();
                for (Object3DInt object3DInt : toKeep) {
                    population.addObject(object3DInt);
                }
            }
        }

        return population;
    }
}
