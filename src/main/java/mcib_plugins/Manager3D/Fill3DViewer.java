package mcib_plugins.Manager3D;

import ij.IJ;
import ij.ImagePlus;
import ij.gui.Toolbar;
import ij3d.*;
import mcib3d.geom2.Object3DInt;
import mcib3d.geom2.Object3DIntLabelImage;
import mcib3d.geom2.Objects3DIntPopulation;
import mcib3d.image3d.ImageByte;
import mcib3d.image3d.ImageHandler;
import mcib3d.image3d.ImageShort;
import org.scijava.vecmath.Color3f;

import java.awt.*;
import java.util.List;

public class Fill3DViewer {
    private Image3DUniverse universe;

    public Fill3DViewer() {
        getUniverse();
    }

    // NOT USED
    private void add3DViewer(Object3DInt obj, String name, Color col) {
        Color3f col3f = new Color3f(col.getRed() / 255.0f, col.getGreen() / 255.0f, col.getBlue() / 255.0f);
        if (obj.size() > 0) {
            if (!universe.contains(name)) {
                ImageHandler labelImage = new Object3DIntLabelImage(obj).getLabelImage(1);
                ImageByte imageByte = ((ImageShort) labelImage).convertToByte(false);
                ImagePlus imagePlus = imageByte.getImagePlus();
                Content c = ContentCreator.createContent(name, imagePlus, 2, 1, 0, col3f, 0, new boolean[]{true, true, true});
                universe.addContentLater(c);
                c.setLocked(true);
            } // already exists
            else {

                System.out.println("Recoloring obj " + name);
                Content surface = universe.getContent(name);
                surface.setColor(col3f);
            }
        }
        //Object3D_IJUtils.computeMeshSurface(obj,true);
    }

    private void getUniverse() {
        System.out.println("Getting universes");
        IJ.log(ImageJ_3D_Viewer.getJava3DVersion());
        List<Image3DUniverse> Viewers3D = Image3DUniverse.universes;
        System.out.println("Universes opened " + Viewers3D.size());
        if (!Viewers3D.isEmpty()) {
            universe = Viewers3D.get(0);
            //universe.show();
        } else {
            universe = new Image3DUniverse(512, 512);
            IJ.wait(100);
            //universe.show();
        }

        System.out.println("Universe " + universe + " " + universe.allContentsString());
    }

    public void fill3DViewer(Object3DInt object3DInt) {
        // Color
        Color color = Toolbar.getForegroundColor();
        Color3f col3f = new Color3f(color.getRed() / 255.0f, color.getGreen() / 255.0f, color.getBlue() / 255.0f);
        fill3DViewer(object3DInt, col3f);
    }

    public void fill3DViewerMacro(Object3DInt object3DInt) {
        // Color
        int intColor = (int) IJ.getImage().getProcessor().getForegroundValue();
        Color color = new Color(intColor);
        System.out.println("Color " + color + " " + intColor);
        Color3f col3f = new Color3f(color.getRed() / 255.0f, color.getGreen() / 255.0f, color.getBlue() / 255.0f);
        fill3DViewer(object3DInt, col3f);
    }


    public void fill3DViewer(Object3DInt object3DInt, Color3f col3f) {
        // Universe
        if (universe == null || universe.getWindow() == null || universe.getCanvas() == null) {
            universe = new Image3DUniverse();
            universe.show();
        }
        ImageHandler labels = new Object3DIntLabelImage(object3DInt).getLabelImage(255);
        ImagePlus imagePlus = labels.thresholdAboveExclusive(0).getImagePlus();
        Content c = ContentCreator.createContent("obj-" + object3DInt.getLabel() + "-" + object3DInt.getType(), imagePlus, 2, 1, 0, col3f, 0, new boolean[]{true, true, true});
        universe.addContentLater(c);
        c.setLocked(true);


        ImageCanvas3D canvas = (ImageCanvas3D) universe.getCanvas();
        canvas.render();
    }


    public void fill3DViewer(List<Object3DInt> object3DInts) {
        // Color
        Color color = Toolbar.getForegroundColor();
        Color3f col3f = new Color3f(color.getRed() / 255.0f, color.getGreen() / 255.0f, color.getBlue() / 255.0f);
        // Universe
        if (universe == null || universe.getWindow() == null || universe.getCanvas() == null) {
            universe = new Image3DUniverse();
            universe.show();
        }
        // create population
        Objects3DIntPopulation population = new Objects3DIntPopulation();
        object3DInts.forEach(population::addObject);
        ImageHandler labels = population.drawImage();
        ImageByte binary = labels.thresholdAboveExclusive(0);
        ImagePlus imagePlus = binary.getImagePlus();
        Content c = ContentCreator.createContent("pop-" + (int) (Math.random() * 1000), imagePlus, 2, 1, 0, col3f, 0, new boolean[]{true, true, true});
        universe.addContentLater(c);
        c.setLocked(true);

        ImageCanvas3D canvas = (ImageCanvas3D) universe.getCanvas();
        canvas.render();
    }
}
