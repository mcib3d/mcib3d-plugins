package mcib_plugins.Manager3D;

import ij.Prefs;
import mcib3d.geom2.Object3DInt;
import mcib3d.geom2.measurements.*;
import mcib3d.image3d.ImageHandler;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class Manager3DMeasurements {

    public static ResultsFrame measurements3D(List<Object3DInt> object3DList) {
        if (object3DList == null) return null;
        List<String> headings = new ArrayList<>();
        headings.add("Nb");
        headings.add("Name");
        headings.add("Label");
        headings.add("Type");
        List<String> headingsMeasure = new ArrayList<>();
        if (Prefs.get("RoiManager3D-V3-Options_centroid.boolean", false)) {
            Collections.addAll(headingsMeasure, new MeasureCentroid().getNamesMeasurement());
        }
        if (Prefs.get("RoiManager3D-V3-Options_volume.boolean", true)) {
            Collections.addAll(headingsMeasure, new MeasureVolume().getNamesMeasurement());
        }
        if (Prefs.get("RoiManager3D-V3-Options_surface.boolean", false)) {
            Collections.addAll(headingsMeasure, new MeasureSurface().getNamesMeasurement());
        }
        if (Prefs.get("RoiManager3D-V3-Options_compactness.boolean", false)) {
            Collections.addAll(headingsMeasure, new MeasureCompactness().getNamesMeasurement());
        }
        if (Prefs.get("RoiManager3D-V3-Options_feret.boolean", false)) {
            Collections.addAll(headingsMeasure, new MeasureFeret().getNamesMeasurement());
        }
        if (Prefs.get("RoiManager3D-V3-Options_ellipse.boolean", false)) {
            Collections.addAll(headingsMeasure, new MeasureEllipsoid().getNamesMeasurement());
        }
        if (Prefs.get("RoiManager3D-V3-Options_dist2Surf.boolean", false)) {
            Collections.addAll(headingsMeasure, new MeasureDistancesCenter().getNamesMeasurement());
        }

        List<String> finalHeadings = headingsMeasure.stream().filter(h -> !h.equalsIgnoreCase("labelobj")).collect(Collectors.toList());
        final Object[][] data = new Object[object3DList.size()][headings.size() + finalHeadings.size()];

        Object3DInt obj;
        for (int i = 0; i < object3DList.size(); i++) {
            obj = object3DList.get(i);
            data[i][0] = i;
            data[i][1] = obj.getName();
            data[i][2] = obj.getLabel();
            data[i][3] = obj.getType();
            MeasureObject measureObject = new MeasureObject(obj);
            final int ii = i;
            AtomicInteger ai = new AtomicInteger(4);
            AtomicInteger aim = new AtomicInteger(0);
            Double[] measurements = measureObject.measureList(finalHeadings.toArray(new String[0]));
            finalHeadings.forEach(h -> {
                data[ii][ai.getAndIncrement()] = measurements[aim.getAndIncrement()];
            });
        }

        // JTABLE
        //Create and set up the window.
        String[] heads = new String[headings.size() + finalHeadings.size()];
        headings.addAll(finalHeadings);
        heads = headings.toArray(heads);
        ResultsFrame tableResultsMeasure = new ResultsFrame("3D Measure", heads, data, ResultsFrame.OBJECT_1);
        tableResultsMeasure.setObjectsList(object3DList);

        return tableResultsMeasure;
    }

    public static ResultsFrame quantif3D(List<Object3DInt> object3DList, ImageHandler ima) {
        List<String> headings = new ArrayList<>();
        headings.add("Nb");
        headings.add("Name");
        headings.add("Label");
        headings.add("Type");
        List<String> headingsMeasure = new ArrayList<>();
        if (Prefs.get("RoiManager3D-V3-Options_Intensity.boolean", true)) {
            Collections.addAll(headingsMeasure, new MeasureIntensity().getNamesMeasurement());
        }
        if (Prefs.get("RoiManager3D-V3-Options_Intensity-hist.boolean", true)) {
            Collections.addAll(headingsMeasure, new MeasureIntensityHist().getNamesMeasurement());
        }

        List<String> finalHeadings = headingsMeasure.stream().filter(h -> !h.equalsIgnoreCase("labelobj")).collect(Collectors.toList());
        final Object[][] data = new Object[object3DList.size()][headings.size() + finalHeadings.size()];
        Object3DInt obj;

        for (int i = 0; i < object3DList.size(); i++) {
            obj = object3DList.get(i);
            data[i][0] = i;
            data[i][1] = obj.getName();
            data[i][2] = obj.getLabel();
            data[i][3] = obj.getType();
            final int ii = i;
            AtomicInteger ai = new AtomicInteger(4);
            MeasureObject measureObject = new MeasureObject(obj);
            finalHeadings.forEach(h -> {
                data[ii][ai.getAndIncrement()] = measureObject.measureIntensity(h, ima);
            });
        }

        // JTABLE
        //Create and set up the window.
        String[] heads = new String[headings.size() + finalHeadings.size()];
        headings.addAll(finalHeadings);
        heads = headings.toArray(heads);

        return new ResultsFrame("3D Quantif", heads, data, ResultsFrame.OBJECT_1);
    }
}
