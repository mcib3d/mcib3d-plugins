package mcib_plugins.Manager3D;

import ij.ImagePlus;
import ij.Prefs;
import ij.WindowManager;
import mcib3d.geom2.Object3DInt;
import mcib3d.geom2.Objects3DIntPopulation;
import mcib3d.image3d.ImageFloat;
import mcib3d.image3d.ImageHandler;
import mcib3d.image3d.ImageShort;

import java.util.List;

public class ExportSelected {
    Objects3DIntPopulation population;

    public ExportSelected() {
    }

    public ImageHandler exportSelected(List<Object3DInt> list) {
        Objects3DIntPopulation population = new Objects3DIntPopulation();
        list.forEach(population::addObject);
        // option reset label
        if (Prefs.get("RoiManager3D-V3-Options_resetLabels.boolean", true))
            population.resetLabels();
        // FIXME option 32-bit
        boolean a32 = Prefs.get("RoiManager3D-V3-Options_seg32Export.boolean", false);
        // option current size
        boolean currentSize = Prefs.get("RoiManager3D-V3-Options_currentSize.boolean", false);
        if (currentSize) {
            ImageHandler handler;
            ImagePlus plus = WindowManager.getCurrentImage();
            if (plus != null) {
                if (a32) handler = new ImageFloat("export", plus.getWidth(), plus.getHeight(), plus.getNSlices());
                else handler = new ImageShort("export", plus.getWidth(), plus.getHeight(), plus.getNSlices());
                population.drawInImage(handler);
                return handler;
            }
        }

        return population.drawImage();
    }
}
