package mcib_plugins.Manager3D;

import ij.IJ;
import ij.ImagePlus;
import ij.ImageStack;
import ij.WindowManager;
import ij.gui.Toolbar;
import ij.process.ImageProcessor;
import mcib3d.geom2.Object3DInt;
import mcib3d.geom2.Object3DPlane;
import mcib3d.geom2.VoxelInt;
import mcib3d.image3d.ImageHandler;

import java.awt.*;
import java.util.List;

public class Fill3DStack {

    public void fill3DStack(List<Object3DInt> object3DInts) {
        // Color
        Color col = Toolbar.getForegroundColor();
        fill3D(object3DInts, col);
    }


    public void fill3D(List<Object3DInt> object3DInts, Color col) {
        // current image to fill in
        ImagePlus ima = WindowManager.getCurrentImage();
        if (ima == null) return;
        if (ima.isHyperStack()) {
            IJ.log("3D Fill does not work with hyperstack");
            return;
        }
        if (ima.getBitDepth() != 24) {
            float gray = (int) Math.round(col.getRed() * 0.3 + col.getGreen() * 0.6 + col.getBlue() * 0.1);
            ImageHandler handler = ImageHandler.wrap(ima);
            for (Object3DInt object3DInt : object3DInts) {
                object3DInt.drawObject(handler, gray);
            }
        } else {
            ImageStack stack = ima.getImageStack();
            for (Object3DInt object3DInt : object3DInts) {
                for (Object3DPlane object3DPlane : object3DInt.getObject3DPlanes()) {
                    ImageProcessor tmp = stack.getProcessor(object3DPlane.getZPlane() + 1);
                    tmp.setColor(col);
                    for (VoxelInt voxel : object3DPlane.getVoxels()) {
                        tmp.drawPixel(voxel.getX(), voxel.getY());
                    }
                }
            }
        }
        ima.updateAndDraw();
    }

    public void fill3D(Object3DInt object3DInt, Color col) {
        // current image to fill in
        ImagePlus ima = WindowManager.getCurrentImage();
        if (ima == null) return;
        if (ima.isHyperStack()) {
            IJ.log("3D Fill does not work with hyperstack");
            return;
        }
        if (ima.getBitDepth() != 24) {
            float gray = (int) Math.round(col.getRed() * 0.3 + col.getGreen() * 0.6 + col.getBlue() * 0.1);
            ImageHandler handler = ImageHandler.wrap(ima);
            object3DInt.drawObject(handler, gray);
        } else {
            ImageStack stack = ima.getImageStack();
            for (Object3DPlane object3DPlane : object3DInt.getObject3DPlanes()) {
                ImageProcessor tmp = stack.getProcessor(object3DPlane.getZPlane() + 1);
                tmp.setColor(col);
                for (VoxelInt voxel : object3DPlane.getVoxels()) {
                    tmp.drawPixel(voxel.getX(), voxel.getY());
                }
            }

        }
        ima.updateAndDraw();
    }
}
