package mcib_plugins.Manager3D;

import mcib3d.geom2.Object3DInt;

import javax.swing.*;
import java.awt.*;

public class ObjectCellRenderer extends DefaultListCellRenderer {
    public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
        super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
        if (value instanceof Object3DInt) {
            Object3DInt object3DInt = (Object3DInt) value;
            if (object3DInt.getType() > 0)
                setText(object3DInt.getName() + "(" + object3DInt.getLabel() + "-" + object3DInt.getType() + ")");
            else setText(object3DInt.getName() + "(" + object3DInt.getLabel() + ")");
        }
        return this;
    }
}
