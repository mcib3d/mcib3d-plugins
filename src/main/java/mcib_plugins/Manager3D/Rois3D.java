package mcib_plugins.Manager3D;

import ij.gui.Roi;

public class Rois3D {
    int zmin = -1;
    int zmax = -1;

    Roi[] arrayRoi;

    public Rois3D(int zmin, int zmax) {
        this.zmin = zmin;
        this.zmax = zmax;

        arrayRoi = new Roi[zmax - zmin + 1];
    }

    public void setRoi(Roi roi, int z) {
        if (checkZRange(z)) {
            arrayRoi[z - zmin] = roi;
        }
    }

    public Roi getRoi(int z) {
        if (checkZRange(z)) {
            return arrayRoi[z - zmin];
        }

        return null;
    }

    public boolean checkZRange(int z) {
        if (z < zmin) return false;
        return z <= zmax;
    }
}
