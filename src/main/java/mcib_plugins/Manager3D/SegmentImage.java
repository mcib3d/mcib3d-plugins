package mcib_plugins.Manager3D;

import ij.Prefs;
import mcib3d.geom2.Objects3DIntPopulation;
import mcib3d.geom2.Objects3DIntPopulationComputation;
import mcib3d.image3d.ImageHandler;
import mcib3d.image3d.ImageLabeller;

public class SegmentImage {
    ImageHandler image;

    public SegmentImage(ImageHandler image) {
        this.image = image;
    }

    public ImageHandler getLabelled() {
        double min = Prefs.get("RoiManager3D-V3-Options_MinSegment.double", 0);
        double max = Prefs.get("RoiManager3D-V3-Options_MaxSegment.double", -1);
        boolean excludeXY = Prefs.get("RoiManager3D-V3-Options_excludeXY.boolean", false);
        boolean excludeZ = Prefs.get("RoiManager3D-V3-Options_excludeZ.boolean", false);
        boolean seg32 = Prefs.get("RoiManager3D-V3-Options_seg32.boolean", false);
        if (max == -1) max = Double.MAX_VALUE;
        ImageLabeller labeller = new ImageLabeller(min, max);
        ImageHandler seg = seg32 ? labeller.getLabelsFloat(image) : labeller.getLabels(image);
        // exclude XYZ
        if (excludeXY || excludeZ) {
            Objects3DIntPopulation pop = new Objects3DIntPopulation(seg);
            pop = new Objects3DIntPopulationComputation(pop).getExcludeBorders(image, excludeZ);
            ImageHandler draw = seg.createSameDimensions();
            pop.drawInImage(draw);
            return draw;
        }

        return seg;
    }
}
