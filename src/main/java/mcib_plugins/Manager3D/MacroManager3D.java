package mcib_plugins.Manager3D;

import ij.IJ;
import ij.WindowManager;
import ij.macro.ExtensionDescriptor;
import ij.macro.Functions;
import ij.macro.MacroExtension;
import ij.plugin.PlugIn;
import mcib3d.geom2.Object3DInt;
import mcib3d.geom2.Objects3DIntPopulation;
import mcib3d.geom2.measurements.Measure2Colocalisation;
import mcib3d.geom2.measurements.Measure2Distance;
import mcib3d.geom2.measurements.MeasureObject;
import mcib3d.image3d.ImageHandler;
import org.scijava.vecmath.Color3f;

import java.awt.*;
import java.io.File;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public class MacroManager3D implements MacroExtension, PlugIn {
    private static final Fill3DViewer viewer3D = new Fill3DViewer();
    private static MacroManager3D macroManager3D = null;
    private static Map<Integer, Object3DInt> object3DIntMap = null;

    @Override
    public String handleExtension(String command, Object[] args) {
        switch (command) {
            case "Manager3DV4_ImportImage":
                importImage();
                break;
            case "Manager3DV4_MeasureList":
                listMeasure();
                break;
            case "Manager3DV4_GetClass":
                getType(args);
                break;
            case "Manager3DV4_SetClass":
                setType(args);
                break;
            case "Manager3DV4_Measure":
                measure(args);
                break;
            case "Manager3DV4_MeasureIntensity":
                measureIntensity(args);
                break;
            case "Manager3DV4_3DViewer":
                viewer3D(args);
                break;
            case "Manager3DV4_3DFill":
                fill3D(args);
                break;
            case "Manager3DV4_DistanceList":
                listDistances();
                break;
            case "Manager3DV4_NbObjects":
                nbObjects(args);
                break;
            case "Manager3DV4_Distance2":
                distance3D(args);
                break;
            case "Manager3DV4_ColocList":
                listColocs();
                break;
            case "Manager3DV4_Coloc2":
                coloc3D(args);
                break;
        }

        return null;
    }

    private void listColocs() {
        IJ.log("---------- Colocalisation -----------");
        String[] names = new Measure2Colocalisation(new Object3DInt(), new Object3DInt()).getNamesMeasurement(); // FIXME with empty constructors
        Arrays.stream(names).sorted().forEach(IJ::log);
    }

    private void coloc3D(Object[] args) {
        Double D = (Double) args[0];
        int a = D.intValue();
        D = (Double) args[1];
        int b = D.intValue();
        String mes = (String) args[2];
        ((Double[]) args[3])[0] = new Measure2Colocalisation(object3DIntMap.get(a), object3DIntMap.get(b)).getValueMeasurement(mes);
    }

    private void fill3D(Object[] args) {
        Double D = (Double) args[0];
        int a = D.intValue();
        D = (Double) args[1];
        int r = D.intValue();
        D = (Double) args[2];
        int g = D.intValue();
        D = (Double) args[3];
        int b = D.intValue();
        Color col = new Color(r / 255.0f, g / 255.0f, b / 255.0f);
        new Fill3DStack().fill3D(object3DIntMap.get(a), col);
    }

    private void nbObjects(Object[] args) {
        ((Double[]) args[0])[0] = (double) object3DIntMap.size();
    }

    private void importImage() {
        Objects3DIntPopulation population = new Objects3DIntPopulation(ImageHandler.wrap(WindowManager.getCurrentImage()));
        object3DIntMap = new HashMap<>();
        AtomicInteger ai = new AtomicInteger(0);
        population.getObjects3DInt().forEach(object3DInt -> object3DIntMap.put(ai.getAndIncrement(), object3DInt));
    }

    private void listMeasure() {
        IJ.log("---------- Measure Geometry -----------");
        new MeasureObject().listMeasures().stream().sorted().forEach(IJ::log);
        IJ.log("---------- Measure Intensity -----------");
        new MeasureObject().listMeasuresIntensity().stream().sorted().forEach(IJ::log);
    }

    private void getType(Object[] args) { // getClass
        Double D = (Double) args[0];
        int a = D.intValue();
        ((Double[]) args[1])[0] = (double) object3DIntMap.get(a).getType();
    }

    private void setType(Object[] args) { // setClass
        Double D = (Double) args[0];
        int a = D.intValue();
        D = (Double) args[1];
        int b = D.intValue();
        object3DIntMap.get(a).setType(b);
    }

    private void measure(Object[] args) {
        Double D = (Double) args[0];
        int a = D.intValue();
        String S = (String) args[1];
        ((Double[]) args[2])[0] = new MeasureObject(object3DIntMap.get(a)).measure(S);
    }

    private void measureIntensity(Object[] args) {
        Double D = (Double) args[0];
        int a = D.intValue();
        String S = (String) args[1];
        ((Double[]) args[2])[0] = new MeasureObject(object3DIntMap.get(a)).measureIntensity(S, ImageHandler.wrap(WindowManager.getCurrentImage()));
    }

    private void viewer3D(Object[] args) {
        Double D = (Double) args[0];
        int a = D.intValue();
        D = (Double) args[1];
        int r = D.intValue();
        D = (Double) args[2];
        int g = D.intValue();
        D = (Double) args[3];
        int b = D.intValue();
        Color3f col3f = new Color3f(r / 255.0f, g / 255.0f, b / 255.0f);
        viewer3D.fill3DViewer(object3DIntMap.get(a), col3f);
    }

    private void distance3D(Object[] args) {
        Double D = (Double) args[0];
        int a = D.intValue();
        D = (Double) args[1];
        int b = D.intValue();
        String mes = (String) args[2];
        ((Double[]) args[3])[0] = new Measure2Distance(object3DIntMap.get(a), object3DIntMap.get(b)).getValueMeasurement(mes);
    }

    private void listDistances() {
        IJ.log("---------- Distances -----------");
        String[] names = new Measure2Distance(new Object3DInt(), new Object3DInt()).getNamesMeasurement(); // FIXME with empty constructors
        Arrays.stream(names).sorted().forEach(IJ::log);
    }


    @Override
    public ExtensionDescriptor[] getExtensionFunctions() {
        int[] argGetType = {ARG_NUMBER, ARG_OUTPUT + ARG_NUMBER};
        int[] argSetType = {ARG_NUMBER, ARG_NUMBER};
        int[] argMeasure = {ARG_NUMBER, ARG_STRING, ARG_OUTPUT + ARG_NUMBER};
        int[] argViewer = {ARG_NUMBER, ARG_NUMBER, ARG_NUMBER, ARG_NUMBER};
        int[] argNb = {ARG_NUMBER + ARG_OUTPUT};
        int[] argDist = {ARG_NUMBER, ARG_NUMBER, ARG_STRING, ARG_NUMBER + ARG_OUTPUT};

        ExtensionDescriptor[] extensions = {
                ExtensionDescriptor.newDescriptor("Manager3DV4_ImportImage", this),
                ExtensionDescriptor.newDescriptor("Manager3DV4_MeasureList", this),
                ExtensionDescriptor.newDescriptor("Manager3DV4_GetClass", this, argGetType),
                ExtensionDescriptor.newDescriptor("Manager3DV4_SetClass", this, argSetType),
                ExtensionDescriptor.newDescriptor("Manager3DV4_Measure", this, argMeasure),
                ExtensionDescriptor.newDescriptor("Manager3DV4_MeasureIntensity", this, argMeasure),
                ExtensionDescriptor.newDescriptor("Manager3DV4_3DViewer", this, argViewer),
                ExtensionDescriptor.newDescriptor("Manager3DV4_3DFill", this, argViewer),
                ExtensionDescriptor.newDescriptor("Manager3DV4_NbObjects", this, argNb),
                ExtensionDescriptor.newDescriptor("Manager3DV4_DistanceList", this),
                ExtensionDescriptor.newDescriptor("Manager3DV4_Distance2", this, argDist),
                ExtensionDescriptor.newDescriptor("Manager3DV4_ColocList", this),
                ExtensionDescriptor.newDescriptor("Manager3DV4_Coloc2", this, argDist)
        };

        return extensions;
    }

    @Override
    public void run(String s) {
        if (IJ.macroRunning()) {
            Functions.registerExtensions(this);
            if (macroManager3D == null) {
                macroManager3D = new MacroManager3D();
            }
        } else {
            IJ.log("Implementing 3D Manager macros.");
            IJ.open(IJ.getDirectory("plugins") + File.separator + "mcib3d-suite" + File.separator + "MacroV4.ijm");
        }
    }
}
