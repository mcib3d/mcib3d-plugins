package mcib_plugins.Manager3D;

import ij.ImagePlus;
import ij.Prefs;
import ij.gui.Roi;
import ij.plugin.filter.ThresholdToSelection;
import ij.process.ByteProcessor;
import ij.process.ImageProcessor;
import mcib3d.geom.ObjectCreator3D;
import mcib3d.geom.Point3D;
import mcib3d.geom2.BoundingBox;
import mcib3d.geom2.Object3DInt;
import mcib3d.geom2.Object3DIntLabelImage;
import mcib3d.geom2.measurements.MeasureCentroid;
import mcib3d.image3d.ImageByte;
import mcib3d.image3d.ImageHandler;

import javax.swing.*;

public class Create3DRoi {
    public static final int ROI_CONTOUR = 0;
    public static final int ROI_BB = 1;
    public static final int ROI_CENTROID = 2;
    public static final int ROI_SPHERE = 3;

    public static Rois3D computeRois(Object3DInt object3DInt) {
        return computeRois(object3DInt, (int) Prefs.get("RoiManager3D-Options_roi.double", 0));
    }

    public static Rois3D computeRois(Object3DInt object3DInt, int roi) {
        ImageHandler labelImageCrop = new Object3DIntLabelImage(object3DInt).getCroppedLabelImage(255);
        int offX = labelImageCrop.offsetX;
        int offY = labelImageCrop.offsetY;
        int offZ = labelImageCrop.offsetZ;
        // draw objects
        ObjectCreator3D creator3DCrop = new ObjectCreator3D(labelImageCrop);
        switch (roi) {
            case ROI_CONTOUR: // CONTOUR already in labelImageCrop
                break;
            case ROI_SPHERE: // SPHERE
                labelImageCrop.fill(0);
                Point3D point3D = new MeasureCentroid(object3DInt).getCentroidAsPoint();
                double rad = Prefs.get("RoiManager3D-V3-Options_radiusRoi.double", 2);
                double radius = Math.max(1, rad);
                radius = Math.min(radius, 50);
                creator3DCrop.createEllipsoid(point3D.getRoundX() - offX, point3D.getRoundY() - offY, point3D.getRoundZ() - offZ, radius, radius, radius, 255, false);
                break;
            case ROI_BB: // BOUNDING BOX
                labelImageCrop.fill(0);
                BoundingBox box = object3DInt.getBoundingBox();
                for (int z = box.zmin; z <= box.zmax; z++) {
                    creator3DCrop.createBrick((int) Math.round(0.5 * (box.xmin + box.xmax) - offX), (int) Math.round(0.5 * (box.ymin + box.ymax) - offY), (int) Math.round(0.5 * (box.zmin + box.zmax) - offZ), 0.5 * (box.xmax - box.xmin), 0.5 * (box.ymax - box.ymin), 0.5 * (box.zmax - box.zmin), 255);
                }
                break;
            case ROI_CENTROID: // CENTROID
                labelImageCrop.fill(0);
                Point3D centre2 = new MeasureCentroid(object3DInt).getCentroidAsPoint();
                creator3DCrop.createPixel(centre2.getRoundX() - offX, centre2.getRoundY() - offY, centre2.getRoundZ() - offZ, 255);
                break;
        }

        Rois3D arrayRoi = new Rois3D(offZ, labelImageCrop.sizeZ + offZ);

        // extract selections crop
        ImageByte binaryCrop = labelImageCrop.thresholdAboveExclusive(0);
        for (int zz = 0; zz < labelImageCrop.sizeZ; zz++) {
            ByteProcessor mask = new ByteProcessor(binaryCrop.sizeX, binaryCrop.sizeY, (byte[]) binaryCrop.getArray1D(zz));
            mask.setThreshold(1, 255, ImageProcessor.NO_LUT_UPDATE);
            ImagePlus maskPlus = new ImagePlus("mask " + zz, mask);
            ThresholdToSelection tts = new ThresholdToSelection();
            tts.setup("", maskPlus);
            tts.run(mask);
            Roi roiTmp = maskPlus.getRoi();
            if (roiTmp != null) {
                roiTmp.setLocation(roiTmp.getXBase() + offX, roiTmp.getYBase() + offY);
            }
            arrayRoi.setRoi(roiTmp, zz + offZ);
        }

        return arrayRoi;
    }


    public Roi[] computeRois(JList listUI, ImagePlus imp) {
        if (imp == null) {
            return null;
        }

        DefaultListModel<Object3DInt> model = (DefaultListModel<Object3DInt>) listUI.getModel();

        int zmin = imp.getNSlices() + 1;
        int zmax = -1;

        int[] indexes = listUI.getSelectedIndices();
        boolean allIndices = indexes.length == 0;
        int nb = allIndices ? model.getSize() : indexes.length;

        // get zmin and zmax
        Object3DInt obj;
        for (int i = 0; i < nb; i++) {
            if (allIndices) obj = model.elementAt(i);
            else obj = model.getElementAt(indexes[i]);
            BoundingBox box = obj.getBoundingBox();
            if (box.zmin < zmin) {
                zmin = box.zmin;
            }
            if (box.zmax > zmax) {
                zmax = box.zmax;
            }
        }

        // draw objects
        ImageHandler label = new ImageByte("rois", imp.getWidth(), imp.getHeight(), imp.getNSlices());
        label.fill(0);
        ObjectCreator3D creator3D = new ObjectCreator3D(label);
        int roi = (int) Prefs.get("RoiManager3D-Options_roi.double", 0);
        for (int index : indexes) {
            //IJ.showStatus("Drawing Rois " + i + " / " + indexes.length);
            obj = model.getElementAt(index);
            switch (roi) {
                case 0: // CONTOUR
                    obj.drawObject(creator3D.getImageHandler(), 255);
                    break;
                case 1: // SPHERE
                    Point3D point3D = new MeasureCentroid(obj).getCentroidAsPoint();
                    creator3D.createEllipsoid(point3D.getRoundX(), point3D.getRoundY(), point3D.getRoundZ(), 2, 2, 1, 255, false);
                    break;
                // POINT
                case 3: // BOUNDING BOX
                    BoundingBox box = obj.getBoundingBox();
                    for (int z = box.zmin; z <= box.zmax; z++) {
                        creator3D.createBrick((int) Math.round(0.5 * (box.xmin + box.xmax)), (int) Math.round(0.5 * (box.ymin + box.ymax)), (int) Math.round(0.5 * (box.zmin + box.zmax)), 0.5 * (box.xmax - box.xmin), 0.5 * (box.ymax - box.ymin), 0.5 * (box.zmax - box.zmin), 255);
                    }
                    break;
                default:
                    Point3D centre2 = new MeasureCentroid(obj).getCentroidAsPoint();
                    creator3D.createPixel(centre2.getRoundX(), centre2.getRoundY(), centre2.getRoundZ(), 255);
                    break;
            }
        }
        //draw.show("draw");

        Roi[] arrayRois = new Roi[imp.getNSlices()];
        // extract selections
        for (int zz = zmin; zz <= zmax; zz++) {
            //IJ.showStatus("Computing Rois " + zz + " / " + zmax);
            ByteProcessor mask = new ByteProcessor(imp.getWidth(), imp.getHeight(), (byte[]) label.getArray1D(zz));
            mask.setThreshold(1, 255, ImageProcessor.NO_LUT_UPDATE);
            ImagePlus maskPlus = new ImagePlus("mask " + zz, mask);
            ThresholdToSelection tts = new ThresholdToSelection();
            tts.setup("", maskPlus);
            tts.run(mask);
            arrayRois[zz] = maskPlus.getRoi();
        }

        return arrayRois;
    }
}
