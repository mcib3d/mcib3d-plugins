package mcib_plugins.Manager3D;

import ij.ImagePlus;
import ij.Prefs;
import ij.WindowManager;
import mcib3d.geom2.Object3DInt;
import mcib3d.geom2.Objects3DIntPopulation;
import mcib3d.image3d.ImageFloat;
import mcib3d.image3d.ImageHandler;
import mcib3d.image3d.ImageShort;

import java.util.LinkedList;
import java.util.List;

public class ExportToImage {


    public ExportToImage() {
    }

    public List<ImageHandler> exportSelected(List<Object3DInt> selectedObjects) {
        List<ImageHandler> imagesExport = new LinkedList<>();
        // option 32-bit
        boolean a32 = Prefs.get("RoiManager3D-V3-Options_seg32Export.boolean", false);
        // option current size
        boolean currentSize = Prefs.get("RoiManager3D-V3-Options_currentSize.boolean", false);
        // option types
        boolean types = Prefs.get("RoiManager3D-V3-Options_typesExport.boolean", false);
        if (types) { // export each type in a different image
            for (int t = 1; t <= 9; t++) {
                final int type = t;
                Objects3DIntPopulation populationType = new Objects3DIntPopulation();
                selectedObjects.stream().filter(obj -> obj.getType() == type).forEach(populationType::addObject);
                // option reset label
                if (Prefs.get("RoiManager3D-V3-Options_resetLabels.boolean", true)) populationType.resetLabels();
                if (populationType.getNbObjects() > 0) {
                    ImageHandler exportImage = drawPopulation(populationType, a32, currentSize);
                    exportImage.setTitle("ExportType" + t);
                    exportImage.setScale(populationType.getVoxelSizeXY(), populationType.getVoxelSizeZ(), "unit"); // FIXME unit
                    imagesExport.add(exportImage);
                }
            }
        } // else no type export
        else {
            Objects3DIntPopulation population = new Objects3DIntPopulation();
            selectedObjects.forEach(population::addObject);
            // option reset label
            if (Prefs.get("RoiManager3D-V3-Options_resetLabels.boolean", true)) population.resetLabels();
            ImageHandler exportImage = drawPopulation(population, a32, currentSize);
            exportImage.setTitle("ExportAll");
            exportImage.setScale(population.getVoxelSizeXY(), population.getVoxelSizeZ(), "unit"); // FIXME unit
            imagesExport.add(exportImage);
        }

        return imagesExport;
    }

    private ImageHandler drawPopulation(Objects3DIntPopulation population, boolean a32, boolean currentSize) {
        if (currentSize) {
            ImageHandler handler;
            ImagePlus plus = WindowManager.getCurrentImage();
            if (plus != null) {
                if (a32) handler = new ImageFloat("export", plus.getWidth(), plus.getHeight(), plus.getNSlices());
                else handler = new ImageShort("export", plus.getWidth(), plus.getHeight(), plus.getNSlices());
                population.drawInImage(handler);
                return handler;
            }
        }
        return population.drawImage();
    }
}
