package mcib_plugins.Manager3D;

import ij.Prefs;
import mcib3d.geom2.Object3DInt;
import mcib3d.geom2.Objects3DIntPopulation;
import mcib3d.geom2.measurementsPopulation.MeasurePopulationDistance;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Manager3DDistances {

    public static ResultsFrame distances3D(List<Object3DInt> object3DListA, List<Object3DInt> object3DListB, double distMax) {
        if ((object3DListA == null) || (object3DListB == null)) return null;

        Objects3DIntPopulation populationA = new Objects3DIntPopulation();
        object3DListA.forEach(populationA::addObject);
        Objects3DIntPopulation populationB = new Objects3DIntPopulation();
        object3DListB.forEach(populationB::addObject);

        String distMethod = DistancesOptions.distMethods[(int) Prefs.get("RoiManager3D-V3-Options_DistMethod.int", 0)];
        MeasurePopulationDistance populationDistance = new MeasurePopulationDistance(populationA, populationB, distMax, distMethod);

        List<String> headings = new ArrayList<>();
        headings.add("Nb");
        headings.add("Name");
        headings.add("Label");
        headings.add("Type");

        Map<Object3DInt, List<Object>> map = new HashMap<>();
        int maxObj = 1;
        int c = 1;
        for (Object3DInt objA : object3DListA) {
            List<Object> list = new ArrayList<>();
            double[] distCC = populationDistance.getValuesObject1Sorted(objA.getLabel(), true);
            maxObj = Math.max(maxObj, distCC.length / 3);
            list.add(c++); // Nb
            list.add(objA.getName()); // name
            list.add(objA.getLabel()); // label
            list.add(objA.getType()); // type
            for (int i = 0; i < distCC.length; i += 3) {
                Object3DInt objB = populationB.getObjectByLabel((float) distCC[i + 1]);
                list.add(objB.getLabel()); // label B
                list.add(objB.getName()); // name B
                list.add(objB.getType()); // type B
                list.add(distCC[i + 2]); // distance
            }
            map.put(objA, list);
        }

        for (int i = 1; i <= maxObj; i++) {
            headings.add("O" + i); // object label coloc
            headings.add("N" + i); // object name coloc
            headings.add("T" + i); // object type coloc
            headings.add("D" + i); // distance
        }

        // create data
        Object[][] data = new Object[object3DListA.size()][headings.size()];
        for (int i = 0; i < object3DListA.size(); i++) {
            Object3DInt objA = object3DListA.get(i);
            for (int j = 0; j < headings.size(); j++) {
                Object val = 0;
                if (j < map.get(objA).size()) val = map.get(objA).get(j);
                data[i][j] = val;
            }
        }

        ResultsFrame tableResultsMeasure = new ResultsFrame("3D Distances", headings.toArray(new String[0]), data, ResultsFrame.OBJECT_1);
        tableResultsMeasure.setObjectsList(object3DListA);

        return tableResultsMeasure;
    }
}
