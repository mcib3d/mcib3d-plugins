package mcib_plugins.TEST;

import ij.IJ;
import ij.ImagePlus;
import ij.ImageStack;
import ij.plugin.Duplicator;
import ij.plugin.PlugIn;
import ij.plugin.ZProjector;
import ij.process.ImageProcessor;
import mcib3d.geom.Vector3D;
import mcib3d.geom.Voxel3D;
import mcib3d.geom2.Object3DInt;
import mcib3d.geom2.measurements.MeasureCentroid;
import mcib3d.geom2.measurements.MeasureEllipsoid;
import mcib3d.geom2.measurements.MeasureFeret;
import mcib3d.image3d.ImageHandler;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class TranslateCenterOfMass_ implements PlugIn {
    public static void main(String[] args) {
        String nameRaw = "SimulRodGaussian5Stack.tif";
        String nameSeg = "SimulRodGaussian5StackBin.tif";
        //final int radiusCC = 128;
        nameRaw = "20200819_Swt0031.asd.tif";
        nameSeg = "20200819_Swt0031.binary.tif";
        final int radiusCC = 128;
        ImagePlus rawPlus = IJ.openImage("/home/thomas/AMU/Projects/AFMVIDEO/Data/" + nameRaw);
        ImagePlus segPlus = IJ.openImage("/home/thomas/AMU/Projects/AFMVIDEO/Data/" + nameSeg);
        int nFrames = rawPlus.getNFrames();
        // centers of mass
        Map<Integer, Voxel3D> mapCenters = new HashMap<>();
        Map<Integer, Vector3D> mapFeret = new HashMap<>();
        List<Voxel3D> centers = IntStream.rangeClosed(0, nFrames - 1).mapToObj(f -> {
            System.out.println("Processing " + f + " / " + nFrames);
            ImagePlus plus1 = extractCurrentStack(rawPlus, 0, f);
            ImageHandler raw = ImageHandler.wrap(plus1);
            ImagePlus plus2 = extractCurrentStack(segPlus, 0, f);
            ImageHandler seg = ImageHandler.wrap(plus2);
            // object detection
            Object3DInt object3DInt = new Object3DInt(seg);
            // measure center of mass
            MeasureCentroid center = new MeasureCentroid(object3DInt);
            double cx = center.getValueMeasurement(MeasureCentroid.CX_PIX);
            double cy = center.getValueMeasurement(MeasureCentroid.CY_PIX);
            Voxel3D V = new Voxel3D(cx, cy, f, 1);
            mapCenters.put(f, V);
            // Feret for orientation
            MeasureFeret feret = new MeasureFeret(object3DInt);
            //Vector3D vect = new Vector3D(feret.getFeret1(), feret.getFeret2());
            // ellipsoid for orientation
            MeasureEllipsoid ellipsoid = new MeasureEllipsoid(object3DInt);
            Vector3D vect = ellipsoid.getAxis1();
            if (vect.x < 0) vect = vect.multiply(-1);
            mapFeret.put(f, new Vector3D(vect.getNormalizedVector()));
            return V;
        }).collect(Collectors.toList());
        final Voxel3D avg = new Voxel3D(rawPlus.getWidth() / 2, rawPlus.getHeight() / 2, 0, 1);
        // translate + rotate
        ImageStack stackAligned1 = new ImageStack(rawPlus.getWidth(), rawPlus.getHeight());
        Vector3D axisX = new Vector3D(1, 0, 0);
        IntStream.rangeClosed(0, nFrames - 1).forEach(f -> {
            System.out.println("Aligning " + f + " / " + nFrames);
            ImagePlus plus = extractCurrentStack(rawPlus, 0, f);
            ImageProcessor processor = plus.getProcessor();
            // translate
            Voxel3D V = mapCenters.get(f);
            processor.translate(avg.x - V.x, avg.y - V.y);
            // rotate
            Vector3D vect = mapFeret.get(f);
            double ang = vect.angleDegrees(axisX);
            if (vect.y > 0) ang *= -1;
            processor.setInterpolationMethod(ImageProcessor.BICUBIC);
            processor.rotate(ang);
            stackAligned1.addSlice(processor);
        });
        ImagePlus plusTrans = new ImagePlus("Aligned1", stackAligned1);
        IJ.run(plusTrans, "Properties...", "channels=1 slices=1 frames=" + nFrames + " pixel_width=1.0000 pixel_height=1.0000 voxel_depth=1.0000");
        IJ.saveAsTiff(plusTrans, "/home/thomas/AMU/Projects/AFMVIDEO/Data/" + nameRaw + "-aligned1.tif");

        // TEST CC1
        ImageStack stackAligned2 = new ImageStack(rawPlus.getWidth(), rawPlus.getHeight());
        ImagePlus plus0 = extractCurrentStack(plusTrans, 0, 0);
        ImageProcessor ref0 = plus0.getProcessor();
        IntStream.rangeClosed(0, nFrames - 1).forEach(f -> {
            System.out.println("CC1 " + f);
            ImagePlus plus = extractCurrentStack(plusTrans, 0, f);
            ImageProcessor processor = plus.getProcessor();
            processor = bestTranslation(processor, ref0, 10, radiusCC);
            processor = bestRotation(processor, ref0, 15, radiusCC);
            stackAligned2.addSlice(processor);
        });
        ImagePlus plusTrans2 = new ImagePlus("Aligned2", stackAligned2);
        // Z project AVG
        IJ.log("Performing maximum Z-projection");
        ZProjector zProjector = new ZProjector();
        zProjector.setMethod(ZProjector.AVG_METHOD);
        zProjector.setStartSlice(1);
        zProjector.setStopSlice(plusTrans2.getNSlices());
        zProjector.setImage(plusTrans2);
        zProjector.doProjection();
        ImagePlus plusAvg = zProjector.getProjection();
        ImageProcessor avgProcessor = plusAvg.getProcessor();
        plusAvg.show();
        IJ.run(plusTrans2, "Properties...", "channels=1 slices=1 frames=" + nFrames + " pixel_width=1.0000 pixel_height=1.0000 voxel_depth=1.0000");
        IJ.saveAsTiff(plusTrans2, "/home/thomas/AMU/Projects/AFMVIDEO/Data/" + nameRaw + "-aligned2.tif");

        // TEST CC2
        ImageStack stackAligned3 = new ImageStack(rawPlus.getWidth(), rawPlus.getHeight());
        IntStream.rangeClosed(0, nFrames - 1).forEach(f -> {
            System.out.println("CC2 " + f);
            ImagePlus plus = extractCurrentStack(plusTrans2, 0, f);
            ImageProcessor processor = plus.getProcessor();
            processor = bestTranslation(processor, avgProcessor, 5, radiusCC);
            processor = bestRotation(processor, avgProcessor, 5, radiusCC);
            stackAligned3.addSlice(processor);
        });
        ImagePlus plusTrans3 = new ImagePlus("Aligned3", stackAligned3);
        IJ.run(plusTrans3, "Properties...", "channels=1 slices=1 frames=" + nFrames + " pixel_width=1.0000 pixel_height=1.0000 voxel_depth=1.0000");
        IJ.saveAsTiff(plusTrans3, "/home/thomas/AMU/Projects/AFMVIDEO/Data/" + nameRaw + "-aligned3.tif");
    }

    private static ImageProcessor bestTranslation(ImageProcessor ima, ImageProcessor ref, int maxT, int radius) {
        double maxCC = Double.NEGATIVE_INFINITY;
        int bestX = 1000;
        int bestY = 1000;
        ImageProcessor best = ima.duplicate();
        for (int tx = -maxT; tx <= maxT; tx++) {
            for (int ty = -maxT; ty <= maxT; ty++) {
                double cc = correlationValueTranslate(ima, ref, tx, ty, radius);
                if (cc > maxCC) {
                    maxCC = cc;
                    bestX = tx;
                    bestY = ty;
                }
            }
        }
        best.translate(bestX, bestY);
        System.out.println("CC TRANS " + bestX + " " + bestY);

        return best;
    }

    private static ImageProcessor bestRotation(ImageProcessor ima, ImageProcessor ref, int maxAng, int radius) {
        double maxCC = Double.NEGATIVE_INFINITY;
        int bestAng = 1000;
        ImageProcessor best = ima.duplicate();
        for (int ang = -maxAng; ang <= maxAng; ang++) {
            double cc = correlationValueRotate(ima, ref, ang, radius);
            if (cc > maxCC) {
                maxCC = cc;
                bestAng = ang;
            }
        }
        best.setInterpolationMethod(ImageProcessor.BICUBIC);
        best.rotate(bestAng);
        System.out.println("CC ROT " + bestAng);

        return best;
    }

    private static ImagePlus extractCurrentStack(ImagePlus plus, int channelI, int frameI) {
        // check dimensions
        int[] dims = plus.getDimensions();//XYCZT
        int channel = channelI == 0 ? plus.getChannel() : channelI;
        int frame = frameI;
        ImagePlus stack;
        // crop actual frame
        if ((dims[2] > 1) || (dims[4] > 1)) {
            Duplicator duplicator = new Duplicator();
            stack = duplicator.run(plus, channel, channel, 1, dims[3], frame, frame);
        } else stack = plus.duplicate();

        return stack;
    }

    private static double correlationValueTranslate(ImageProcessor ima, ImageProcessor ref, int tx, int ty, int radius) {
        int KernelWidth = ref.getWidth();
        int KernelHeight = ref.getHeight();
        int countpix = 0;
        double kmean = 0;
        double smean = 0;
        double tk = 0;
        double ts = 0;
        double skk = 0;
        double sss = 0;
        double sks = 0;
        double rfactor = 0;
        double tiny = 1.0e-20;
        int xxr;
        int yyr;
        double dist2;
        int radius2;
        if (radius > 0) {
            radius2 = radius * radius;
        } else {
            radius2 = Integer.MAX_VALUE;
        }
        int xc = KernelWidth / 2;
        int yc = KernelHeight / 2;

        for (int xx = 0; xx < KernelWidth; xx++) {
            for (int yy = 0; yy < KernelHeight; yy++) {
                dist2 = (xx - xc) * (xx - xc) + (yy - yc) * (yy - yc);
                if (dist2 < radius2) {
                    xxr = xx + tx;
                    yyr = yy + ty;
                    kmean += ref.getPixelValue(xxr, yyr);
                    smean += ima.getPixelValue(xx, yy);
                    countpix++;
                }
            }
        }
        kmean /= countpix;
        smean /= countpix;
        for (int xx = 0; xx < KernelWidth; xx++) {
            for (int yy = 0; yy < KernelHeight; yy++) {
                dist2 = (xx - xc) * (xx - xc) + (yy - yc) * (yy - yc);
                if (dist2 < radius2) {
                    xxr = xx + tx;
                    yyr = yy + ty;
                    tk = ref.getPixelValue(xxr, yyr) - kmean;
                    ts = ima.getPixelValue(xx, yy) - smean;
                    skk += tk * tk;
                    sss += ts * ts;
                    sks += tk * ts;
                }
            }
        }
        rfactor = sks / (Math.sqrt(skk * sss) + tiny);

        return rfactor;
    }

    private static double correlationValueRotate(ImageProcessor ima, ImageProcessor ref, double ang, int radius) {
        int KernelWidth = ref.getWidth();
        int KernelHeight = ref.getHeight();
        int xc = KernelWidth / 2;
        int yc = KernelHeight / 2;
        int countpix = 0;
        double dist2;
        int radius2;
        if (radius > 0) {
            radius2 = radius * radius;
        } else {
            radius2 = Integer.MAX_VALUE;
        }

        double cosa = Math.cos(Math.toRadians(-ang));
        double sina = Math.sin(Math.toRadians(-ang));
        double kmean = 0;
        double smean = 0;
        double tk = 0;
        double ts = 0;
        double skk = 0;
        double sss = 0;
        double sks = 0;
        double rfactor = 0;
        double tiny = 1.0e-20;
        double xxr;
        double yyr;

        for (int xx = 0; xx < KernelWidth; xx++) {
            for (int yy = 0; yy < KernelHeight; yy++) {
                dist2 = (xx - xc) * (xx - xc) + (yy - yc) * (yy - yc);
                if (dist2 < radius2) {
                    xxr = (xx - xc) * cosa + (yy - yc) * sina + xc;
                    yyr = (yy - yc) * cosa - (xx - xc) * sina + yc;
                    kmean += ref.getInterpolatedPixel(xxr, yyr);
                    smean += ima.getPixelValue(xx, yy);
                    countpix++;
                }
            }
        }
        kmean /= countpix;
        smean /= countpix;
        for (int xx = 0; xx < KernelWidth; xx++) {
            for (int yy = 0; yy < KernelHeight; yy++) {
                dist2 = (xx - xc) * (xx - xc) + (yy - yc) * (yy - yc);
                if (dist2 < radius2) {
                    xxr = (xx - xc) * cosa + (yy - yc) * sina + xc;
                    yyr = (yy - yc) * cosa - (xx - xc) * sina + yc;
                    tk = ref.getInterpolatedPixel(xxr, yyr) - kmean;
                    ts = ima.getPixelValue(xx, yy) - smean;
                    skk += tk * tk;
                    sss += ts * ts;
                    sks += tk * ts;
                }
            }
        }
        rfactor = sks / (Math.sqrt(skk * sss) + tiny);

        return rfactor;
    }

    @Override
    public void run(String s) {
    }

}
