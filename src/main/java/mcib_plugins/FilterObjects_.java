package mcib_plugins;

import ij.ImagePlus;
import ij.Prefs;
import ij.gui.GenericDialog;
import ij.plugin.filter.PlugInFilter;
import ij.process.ImageProcessor;
import mcib3d.geom2.Objects3DIntPopulation;
import mcib3d.geom2.measurements.MeasureObject;
import mcib3d.geom2.measurements.MeasureVolume;
import mcib3d.image3d.ImageHandler;

public class FilterObjects_ implements PlugInFilter {
    private ImagePlus imp;
    private double minValue = Prefs.get("FilterObjects_minValue.double", 0);
    private double maxValue = Prefs.get("FilterObjects_maxValue.double", 1000);
    private boolean keep = false;
    private String measure = MeasureVolume.VOLUME_PIX;

    @Override
    public int setup(String s, ImagePlus imagePlus) {
        this.imp = imagePlus;
        return DOES_8G + DOES_16 + DOES_32;
    }

    @Override
    public void run(ImageProcessor imageProcessor) {
        if (dialog()) {
            // Prefs
            Prefs.set("FilterObjects_minValue.double", minValue);
            Prefs.set("FilterObjects_maxValue.double", maxValue);
            // build population
            ImageHandler imageHandler = ImageHandler.wrap(imp);
            Objects3DIntPopulation population = new Objects3DIntPopulation(imageHandler);
            Objects3DIntPopulation population1 = new Objects3DIntPopulation();
            // filter
            population.getObjects3DInt().forEach(obj -> {
                double value = new MeasureObject(obj).measure(measure);
                if (keep) {
                    if ((value >= minValue) && (value <= maxValue))
                        population1.addObject(obj);
                } else {
                    if ((value < minValue) || (value > maxValue))
                        population1.addObject(obj);
                }
            });
            // results
            ImageHandler filtered = imageHandler.createSameDimensions();
            population1.drawInImage(filtered);
            filtered.show(imageHandler.getTitle() + "_filtered");
        }
    }

    private boolean dialog() {
        String[] measures = new MeasureObject().listMeasures().stream().sorted(String::compareTo).toArray(String[]::new);
        GenericDialog dia = new GenericDialog("Filter Objects");
        dia.addChoice("Descriptor", measures, measure);
        dia.addNumericField("Min value", minValue);
        dia.addNumericField("Max value", maxValue);
        dia.addCheckbox("Keep ? (else discard)", keep);
        dia.showDialog();
        measure = dia.getNextChoice();
        minValue = dia.getNextNumber();
        maxValue = dia.getNextNumber();
        keep = dia.getNextBoolean();

        return dia.wasOKed();
    }
}
