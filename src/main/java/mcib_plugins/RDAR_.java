package mcib_plugins;

import ij.IJ;
import ij.ImagePlus;
import ij.gui.GenericDialog;
import ij.measure.ResultsTable;
import ij.plugin.filter.PlugInFilter;
import ij.process.ImageProcessor;
import mcib3d.geom.RDAR;
import mcib3d.geom2.Object3DInt;
import mcib3d.geom2.Objects3DIntPopulation;
import mcib3d.image3d.ImageFloat;
import mcib3d.image3d.ImageHandler;
import mcib3d.image3d.ImageShort;

import java.util.List;

/**
 * Created by thomasb on 19/7/16.
 */
public class RDAR_ implements PlugInFilter {
    ResultsTable rt;
    boolean display = false;
    private ImagePlus imagePlus;
    private int minVolume = 100;

    @Override
    public int setup(String arg, ImagePlus imp) {
        imagePlus = imp;
        return DOES_8G + DOES_16 + DOES_32;
    }

    @Override
    public void run(ImageProcessor ip) {
        if (dialog()) {
            ImageHandler imageInt = ImageHandler.wrap(imagePlus);
            Objects3DIntPopulation objects3DPopulation = new Objects3DIntPopulation(imageInt);
            // drawing
            ImageHandler drawIn = null;
            ImageHandler drawOut = null;
            ImageHandler drawEll = null;
            if (display) {
                if ((imagePlus.getBitDepth() == 8) || (imagePlus.getBitDepth() == 16)) {
                    drawIn = new ImageShort("Parts inside", imagePlus.getWidth(), imagePlus.getHeight(), imagePlus.getImageStackSize());
                    drawOut = new ImageShort("Parts outside", imagePlus.getWidth(), imagePlus.getHeight(), imagePlus.getImageStackSize());
                    drawEll = new ImageShort("Ellipsoid", imagePlus.getWidth(), imagePlus.getHeight(), imagePlus.getImageStackSize());
                } else if (imagePlus.getBitDepth() == 32) {
                    drawIn = new ImageFloat("Parts inside", imagePlus.getWidth(), imagePlus.getHeight(), imagePlus.getImageStackSize());
                    drawOut = new ImageFloat("Parts outside", imagePlus.getWidth(), imagePlus.getHeight(), imagePlus.getImageStackSize());
                    drawEll = new ImageFloat("Ellipsoid", imagePlus.getWidth(), imagePlus.getHeight(), imagePlus.getImageStackSize());
                }
                drawIn.setScale(imageInt);
                drawOut.setScale(imageInt);
                drawEll.setScale(imageInt);
            }
            // ResultsTable
            rt = ResultsTable.getResultsTable();
            if (rt == null) {
                rt = new ResultsTable();
            }
            rt.reset();
            for (Object3DInt object3D : objects3DPopulation.getObjects3DInt()) {
                processObject(object3D, drawIn, drawOut, drawEll);
            }
            rt.show("Results");
            if (display) {
                drawIn.show("Parts_In");
                drawOut.show("Parts_Out");
                drawEll.show("Ellipsoid");
            }
        }
    }

    private void processObject(Object3DInt object3D, ImageHandler drawIn, ImageHandler drawOut, ImageHandler drawEll) {
        IJ.log("Analysing object " + object3D.getLabel());
        rt.incrementCounter();
        RDAR rdar = new RDAR(object3D);
        rt.addValue("Value", object3D.getLabel());
        rt.addValue("NbIn", rdar.getPartsInNumber(minVolume));
        rt.addValue("NbOut", rdar.getPartsOutNumber(minVolume));
        // volume
        List<Object3DInt> list = rdar.getPartsIn(minVolume);
        int vol = 0;
        if (list != null)
            for (Object3DInt object3DVoxels : list) vol += object3DVoxels.size();
        rt.addValue("volIn", vol);
        list = rdar.getPartsOut(minVolume);
        vol = 0;
        if (list != null)
            for (Object3DInt object3DVoxels : list) vol += object3DVoxels.size();
        rt.addValue("volOut", vol);

        // drawing
        if (display) {
            float color = object3D.getLabel();
            rdar.getEllipsoid().drawObject(drawEll, color);
            if (rdar.getPartsIn(minVolume) != null)
                for (Object3DInt part : rdar.getPartsIn(minVolume)) part.drawObject(drawIn, color);
            color = object3D.getLabel();
            if (rdar.getPartsOut(minVolume) != null)
                for (Object3DInt part : rdar.getPartsOut(minVolume)) part.drawObject(drawOut, color);
        }
    }

    private boolean dialog() {
        GenericDialog genericDialog = new GenericDialog("RDAR");
        genericDialog.addNumericField("Min volume parts", minVolume, 1, 10, "pix");
        genericDialog.addCheckbox("Display images", display);
        genericDialog.showDialog();
        minVolume = (int) genericDialog.getNextNumber();
        display = genericDialog.getNextBoolean();
        return genericDialog.wasOKed();

    }
}