package mcib_plugins;

import ij.IJ;
import ij.ImagePlus;
import ij.WindowManager;
import ij.gui.GenericDialog;
import ij.measure.ResultsTable;
import ij.plugin.PlugIn;
import mcib3d.geom2.Objects3DIntPopulation;
import mcib3d.geom2.measurementsPopulation.MeasurePopulationColocalisation;
import mcib3d.image3d.ImageHandler;

public class Multi_Coloc implements PlugIn {
    @Override
    public void run(String arg) {
        int nbima = WindowManager.getImageCount();
        if (nbima < 1) {
            IJ.error("Needs at least one labelled image");
            return;
        }
        String[] namesA = new String[nbima];
        String[] namesB = new String[nbima];
        for (int i = 0; i < nbima; i++) {
            namesA[i] = WindowManager.getImage(i + 1).getShortTitle();
            namesB[i] = WindowManager.getImage(i + 1).getShortTitle();
        }
        int idxA = 0;
        int idxB = nbima > 1 ? 1 : 0;
        GenericDialog dia = new GenericDialog("MultiColoc");
        dia.addChoice("Image_A", namesA, namesA[idxA]);
        dia.addChoice("Image_B", namesB, namesB[idxB]);
        dia.showDialog();
        if (dia.wasOKed()) {
            idxA = dia.getNextChoiceIndex();
            idxB = dia.getNextChoiceIndex();

            ImagePlus plusA = WindowManager.getImage(idxA + 1);
            ImagePlus plusB = WindowManager.getImage(idxB + 1);

            // NEW
            Objects3DIntPopulation population1 = new Objects3DIntPopulation(ImageHandler.wrap(plusA));
            Objects3DIntPopulation population2 = new Objects3DIntPopulation(ImageHandler.wrap(plusB));
            MeasurePopulationColocalisation colocalisation = new MeasurePopulationColocalisation(population1, population2);
            // Results Table, add percentage coloc
            ResultsTable rt = colocalisation.getResultsTableOnlyColoc(false);
            String[] headings = rt.getHeadings();
            int maxColoc = (headings.length - 1) / 2;
            System.out.println("Coloc max " + maxColoc);
            double[] labels = rt.getColumn("LabelObj");
            for (int o = 1; o <= maxColoc; o++) {
                double[] O = rt.getColumn("O" + o);
                double[] V = rt.getColumn("V" + o);
                for (int i = 0; i < O.length; i++) {
                    double percent = V[i] / population1.getObjectByLabel((float) labels[i]).size();
                    rt.setValue("P" + o, i, percent * 100.0);
                }
            }
            rt.sort("LabelObj");
            rt.show("Colocalisation");


            /*
            Objects3DPopulation popA = new Objects3DPopulation(ImageHandler.wrap(plusA));
            Objects3DPopulation popB = new Objects3DPopulation(ImageHandler.wrap(plusB));

            Objects3DPopulationColocalisation colocalisationOld = new Objects3DPopulationColocalisation(popA, popB);
            if ((tab == 1) || (tab == 2))
                colocalisationOld.getResultsTableAll(true).show("Coloc all");
            if ((tab == 0) || (tab == 2))
                colocalisationOld.getResultsTableOnlyColoc(true).show("Coloc only");
            */
            IJ.log("Finished");
        }
    }
}
