package mcib_plugins;

import ij.IJ;
import ij.ImagePlus;
import ij.gui.GenericDialog;
import ij.plugin.Concatenator;
import ij.plugin.Duplicator;
import ij.plugin.HyperStackConverter;
import ij.plugin.filter.PlugInFilter;
import ij.process.ImageProcessor;
import mcib3d.geom2.tracking.TrackingAssociation;
import mcib3d.image3d.ImageHandler;

public class Association_Tracking implements PlugInFilter {
    private ImagePlus plus;
    private int costChoice = 0;
    private double minColoc = 0.1;
    private double maxBB = 0;
    private boolean trackSlices = false;

    @Override
    public int setup(String s, ImagePlus imagePlus) {
        plus = imagePlus;

        return DOES_8G + DOES_16 + DOES_32 + STACK_REQUIRED;
    }

    @Override
    public void run(ImageProcessor imageProcessor) {
        if (!dialog()) return;

        // Image information
        int[] dims = plus.getDimensions();//XYCZT
        // swap slices and frames in case trackSlices
        ImagePlus plusTrack;
        if (trackSlices) {
            if (dims[3] < 2) IJ.log("Nb slices should be > 2 when using track slices option");
            IJ.log("Dim " + dims[0] + " " + dims[1] + " " + dims[2] + " " + dims[3] + " " + dims[4]);
            plusTrack = HyperStackConverter.toHyperStack(plus.duplicate(), 1, 1, dims[3], "xyzct", "composite");
        } else plusTrack = plus.duplicate();
        // Image information
        int nFrames = plusTrack.getNFrames();
        dims = plusTrack.getDimensions();//XYCZT
        int channel = plusTrack.getChannel();
        int frame = plusTrack.getFrame();
        IJ.log("Dim track " + dims[0] + " " + dims[1] + " " + dims[2] + " " + dims[3] + " " + dims[4]);
        // should work with 4D hyperstacks, extract current frame
        if ((plusTrack.isHyperStack()) || (dims[2] > 1) || (dims[4] > 1)) {
            IJ.log("Hyperstack found, extracting current channel " + channel + " and frame " + frame);
            Duplicator duplicator = new Duplicator();
            // duplicate frames
            ImagePlus plus1 = duplicator.run(plusTrack, channel, channel, 1, dims[3], frame, frame);
            ImagePlus plus2 = duplicator.run(plusTrack, channel, channel, 1, dims[3], frame + 1, frame + 1);
            // ImageHandlers
            ImagePlus result = plus1.duplicate();
            ImageHandler img1 = ImageHandler.wrap(plus1);
            ImageHandler img2 = ImageHandler.wrap(plus2);
            // Association
            TrackingAssociation trackingAssociation = new TrackingAssociation(img1, img2, maxBB, minColoc);
            for (int i = frame + 1; i <= nFrames; i++) {
                IJ.log("Processing " + i);
                ImageHandler tracked = trackingAssociation.getTrackedImage();
                result = Concatenator.run(result, tracked.getImagePlus());
                if (i < nFrames) {
                    trackingAssociation.setImage1(tracked);
                    ImagePlus plusTmp = duplicator.run(plusTrack, channel, channel, 1, dims[3], i + 1, i + 1);
                    trackingAssociation.setImage2(ImageHandler.wrap(plusTmp));
                }
            }
            result = HyperStackConverter.toHyperStack(result, 1, img1.sizeZ, nFrames - frame + 1, "xyzct", "composite");
            result.show("Tracked image");
        }
    }

    private boolean dialog() {
        String[] costChoices = new String[]{"Colocalisation", "Distance Border-Border"};
        GenericDialog dia = new GenericDialog("Association Tracking");
        dia.addMessage("Method for association");
        dia.addChoice("Method", costChoices, costChoices[costChoice]);
        dia.addNumericField("Min coloc (% object in A)", minColoc * 100, 5);
        dia.addNumericField("Max dist BB (pixel unit)", maxBB, 3);
        dia.addCheckbox("Track_slices instead of frames", trackSlices);
        dia.showDialog();
        if (dia.wasOKed()) {
            costChoice = dia.getNextChoiceIndex();
            minColoc = dia.getNextNumber() / 100.0;
            maxBB = dia.getNextNumber();
            trackSlices = dia.getNextBoolean();
        }

        return dia.wasOKed();
    }
}
