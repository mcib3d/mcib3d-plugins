/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mcib_plugins;

import ij.IJ;
import ij.ImagePlus;
import ij.Prefs;
import ij.WindowManager;
import ij.gui.GenericDialog;
import ij.plugin.PlugIn;
import mcib3d.geom2.Objects3DIntPopulation;
import mcib3d.geom2.measurementsPopulation.MeasurePopulationClosestDistance;
import mcib3d.geom2.measurementsPopulation.MeasurePopulationDistance;
import mcib3d.image3d.ImageHandler;

public class Distances_Closest implements PlugIn {
    private final boolean removeZero = false;
    private String measurement = MeasurePopulationDistance.DIST_CC_PIX;
    private double distMax = 1000;
    private int idxA, idxB;
    private double nbClosest = 1;

    @Override
    public void run(String arg) {
        int nbima = WindowManager.getImageCount();
        if (nbima < 1) {
            IJ.error("Needs at least one labelled image");
            return;
        }

        if (dialog()) {
            ImagePlus plusA = WindowManager.getImage(idxA + 1);
            ImagePlus plusB = WindowManager.getImage(idxB + 1);

            // Compute populations
            IJ.log("Computing populations");
            Objects3DIntPopulation population1 = new Objects3DIntPopulation(ImageHandler.wrap(plusA));
            Objects3DIntPopulation population2 = new Objects3DIntPopulation(ImageHandler.wrap(plusB));

            IJ.log("Computing distances");
            // CC PIX
            if (measurement.equals(MeasurePopulationDistance.DIST_CC_PIX)) {
                MeasurePopulationClosestDistance closestDistance;
                if (nbClosest == 1)
                    closestDistance = new MeasurePopulationClosestDistance(population1, population2, distMax, MeasurePopulationClosestDistance.CLOSEST_CC1_PIX);
                else
                    closestDistance = new MeasurePopulationClosestDistance(population1, population2, distMax, MeasurePopulationClosestDistance.CLOSEST_CC2_PIX);
                closestDistance.setRemoveZeroDistance(removeZero);
                // Results Table
                closestDistance.getResultsTableOnlyColoc(true).show("ClosestDistanceCCPix");
            }

            // CC UNIT
            if (measurement.equals(MeasurePopulationDistance.DIST_CC_UNIT)) {
                MeasurePopulationClosestDistance closestDistance;
                if (nbClosest == 1)
                    closestDistance = new MeasurePopulationClosestDistance(population1, population2, distMax, MeasurePopulationClosestDistance.CLOSEST_CC1_UNIT);
                else
                    closestDistance = new MeasurePopulationClosestDistance(population1, population2, distMax, MeasurePopulationClosestDistance.CLOSEST_CC2_UNIT);
                closestDistance.setRemoveZeroDistance(removeZero);
                // Results Table
                closestDistance.getResultsTableOnlyColoc(true).show("ClosestDistanceCCUnit");
            }
            // BB PIX
            if (measurement.equals(MeasurePopulationDistance.DIST_BB_PIX)) {
                MeasurePopulationClosestDistance closestDistance;
                if (nbClosest == 1)
                    closestDistance = new MeasurePopulationClosestDistance(population1, population2, distMax, MeasurePopulationClosestDistance.CLOSEST_BB1_PIX);
                else
                    closestDistance = new MeasurePopulationClosestDistance(population1, population2, distMax, MeasurePopulationClosestDistance.CLOSEST_BB2_PIX);
                closestDistance.setRemoveZeroDistance(removeZero);
                // Results Table
                closestDistance.getResultsTableOnlyColoc(true).show("ClosestDistanceBBPix");
            }

            // BB UNIT
            if (measurement.equals(MeasurePopulationDistance.DIST_BB_UNIT)) {
                MeasurePopulationClosestDistance closestDistance;
                if (nbClosest == 1)
                    closestDistance = new MeasurePopulationClosestDistance(population1, population2, distMax, MeasurePopulationClosestDistance.CLOSEST_BB1_UNIT);
                else
                    closestDistance = new MeasurePopulationClosestDistance(population1, population2, distMax, MeasurePopulationClosestDistance.CLOSEST_BB2_UNIT);
                closestDistance.setRemoveZeroDistance(removeZero);
                // Results Table
                closestDistance.getResultsTableOnlyColoc(true).show("ClosestDistanceBBUnit");
            }

            IJ.log("Finished");
        }
    }

    private boolean dialog() {
        int nbima = WindowManager.getImageCount();
        // choice for results table
        String[] namesA = new String[nbima];
        String[] namesB = new String[nbima];
        for (int i = 0; i < nbima; i++) {
            namesA[i] = WindowManager.getImage(i + 1).getShortTitle();
            namesB[i] = WindowManager.getImage(i + 1).getShortTitle();
        }
        // measurements
        String[] measurements = new String[4];
        measurements[0] = MeasurePopulationDistance.DIST_CC_PIX;
        measurements[1] = MeasurePopulationDistance.DIST_CC_UNIT;
        measurements[2] = MeasurePopulationDistance.DIST_BB_PIX;
        measurements[3] = MeasurePopulationDistance.DIST_BB_UNIT;
        idxA = 0;
        idxB = nbima > 1 ? 1 : 0;
        distMax = Prefs.get("3DSuiteClosest_distMax.double", distMax);

        GenericDialog dia = new GenericDialog("Closest Distances");
        dia.addChoice("Image_A", namesA, namesA[idxA]);
        dia.addChoice("Image_B", namesB, namesB[idxB]);
        dia.addSlider("Number of closest", 1, 2, nbClosest);
        // dia.addCheckbox("Remove zero distance values", removeZero);
        dia.addMessage("Choose distance to use to compute closest");
        dia.addChoice("Distance", measurements, measurement);
        dia.addNumericField("Distance_Maximum", distMax);

        dia.showDialog();

        if (dia.wasOKed()) {
            idxA = dia.getNextChoiceIndex();
            idxB = dia.getNextChoiceIndex();
            nbClosest = dia.getNextNumber();
            // removeZero = dia.getNextBoolean();
            measurement = dia.getNextChoice();
            distMax = dia.getNextNumber();
            // Prefs
            Prefs.set("3DSuiteClosest_distMax.double", distMax);
        }

        return dia.wasOKed();
    }
}
