package mcib_plugins;

import ij.IJ;
import ij.ImagePlus;
import ij.Prefs;
import ij.WindowManager;
import ij.gui.GenericDialog;
import ij.measure.ResultsTable;
import ij.plugin.PlugIn;
import mcib3d.geom2.Object3DInt;
import mcib3d.geom2.Objects3DIntPopulation;
import mcib3d.geom2.measurementsPopulation.MeasurePopulationDistance;
import mcib3d.image3d.ImageHandler;
import mcib3d.image3d.processing.ImageCropper;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class DistancesContainer_ implements PlugIn {
    private int idxA, idxB;
    private double maxDist = 1000;
    private String measurement = MeasurePopulationDistance.DIST_CC_PIX;

    @Override
    public void run(String s) {
        int nbima = WindowManager.getImageCount();
        if (nbima < 2) {
            IJ.error("Requires two labelled image");
            return;
        }

        if (dialog()) {
            IJ.log("");
            IJ.log("Building objects population");
            ImagePlus plusA = WindowManager.getImage(idxA + 1); // spots
            ImagePlus plusB = WindowManager.getImage(idxB + 1); // nuclei - containers
            ImageHandler image1 = ImageHandler.wrap(plusA);
            ImageHandler image2 = ImageHandler.wrap(plusB);
            Objects3DIntPopulation population2 = new Objects3DIntPopulation(image2);

            // Extract containers from image2
            List<Object3DInt> objects3d = population2.getObjects3DInt();
            AtomicInteger row = new AtomicInteger(0);
            // Results Table
            final ResultsTable rt = ResultsTable.getResultsTable() == null ? new ResultsTable() : ResultsTable.getResultsTable();
            rt.reset();
            objects3d.forEach(cont3d -> {
                IJ.log("Computing distances within object " + cont3d.getLabel());
                ImageHandler cropped = new ImageCropper(image1).cropBoundingBoxMaskObject(cont3d);
                Objects3DIntPopulation popCropped = new Objects3DIntPopulation(cropped);
                MeasurePopulationDistance distance = new MeasurePopulationDistance(popCropped, popCropped, maxDist, measurement);
                popCropped.getObjects3DInt().forEach(obj3d -> {
                    double[] dists = distance.getValuesObject1Sorted(obj3d.getLabel(), true);
                    // put values into RT
                    rt.incrementCounter();
                    for (int i = 0; i < dists.length; i += 3) {
                        rt.setValue("LabelCont", row.get(), cont3d.getLabel());
                        rt.setValue("LabelObj", row.get(), dists[i]);
                        rt.setValue("O" + (i + 3) / 3, row.get(), dists[i + 1]);
                        rt.setValue("V" + (i + 3) / 3, row.get(), dists[i + 2]);
                    }
                    row.getAndIncrement();
                });
            });
            rt.show("Distances");
        }
    }

    private boolean dialog() {
        int nbima = WindowManager.getImageCount();
        // choice for results table
        String[] namesA = new String[nbima];
        String[] namesB = new String[nbima];
        for (int i = 0; i < nbima; i++) {
            namesA[i] = WindowManager.getImage(i + 1).getShortTitle();
            namesB[i] = WindowManager.getImage(i + 1).getShortTitle();
        }
        // measurements
        String[] measurements = new String[4];
        measurements[0] = MeasurePopulationDistance.DIST_CC_PIX;
        measurements[1] = MeasurePopulationDistance.DIST_CC_UNIT;
        measurements[2] = MeasurePopulationDistance.DIST_BB_PIX;
        measurements[3] = MeasurePopulationDistance.DIST_BB_UNIT;

        idxA = 0;
        idxB = nbima > 1 ? 1 : 0;

        maxDist = Prefs.get("3DSuiteDistContainer_distMax.double", maxDist);

        GenericDialog dia = new GenericDialog("Distances Inside");
        dia.addChoice("Objects for distances", namesA, namesA[idxA]);
        dia.addChoice("Containers", namesB, namesB[idxB]);
        dia.addChoice("Distance", measurements, measurement);
        dia.addNumericField("Max distance", maxDist);


        dia.showDialog();

        if (dia.wasOKed()) {
            idxA = dia.getNextChoiceIndex();
            idxB = dia.getNextChoiceIndex();
            measurement = dia.getNextChoice();
            maxDist = dia.getNextNumber();

            Prefs.set("3DSuiteDistContainer_distMax.double", maxDist);
        }


        return dia.wasOKed();
    }
}
