package mcib_plugins;

import ij.*;
import ij.gui.GenericDialog;
import ij.measure.Calibration;
import ij.plugin.Duplicator;
import ij.plugin.filter.PlugInFilter;
import ij.process.ImageProcessor;
import mcib3d.image3d.ImageHandler;
import mcib3d.image3d.IterativeThresholding2.TrackThreshold2;
import mcib3d.image3d.processing.FastFilters3D;

public class Track_Threshold2 implements PlugInFilter {
    int volMax = (int) Prefs.get("mcib_iterative_volmax.int", 1000);
    int volMin = (int) Prefs.get("mcib_iterative_volmin.int", 100);
    double minTh = (int) Prefs.get("mcib_iterative_thmin.int", 0);
    int minCont = (int) Prefs.get("mcib_iterative_contmin.int", 0);
    boolean filter = false;
    Calibration cal;
    ImagePlus plus;
    private int step = (int) Prefs.get("mcib_iterative_step.int", 10);
    private int threshold_method = (int) Prefs.get("mcib_iterative_method.int", 0);
    private int crit = (int) Prefs.get("mcib_iterative_criteria.int", 0);
    private int seg = (int) Prefs.get("mcib_iterative_seg.int", 0);
    private boolean start;
    private String[] methods;
    private String[] criteria;
    private String[] segs;
    private int markers = 0;

    @Override
    public int setup(String arg, ImagePlus imp) {
        return DOES_32 + DOES_16 + DOES_8G;
    }

    @Override
    public void run(ImageProcessor ip) {

        plus = IJ.getImage();
        Calibration calibration = plus.getCalibration();

        if (!dialogue()) {
            return;
        }
        // extract current time 
        Duplicator dup = new Duplicator();
        int[] dim = plus.getDimensions();
        int selectedTime = plus.getFrame();
        ImagePlus timeDuplicate = dup.run(plus, 1, 1, 1, dim[3], selectedTime, selectedTime);
        if (filter) {
            int radX = (int) Math.floor(Math.pow((volMin * 3.0) / (4.0 * Math.PI), 1.0 / 3.0));
            if (radX > 10) {
                radX = 10;
            }
            if (radX < 1) {
                radX = 1;
            }
            int radZ = radX; // use calibration ?
            IJ.log("Filtering with radius " + radX);
            ImageStack res = FastFilters3D.filterIntImageStack(timeDuplicate.getStack(), FastFilters3D.MEDIAN, radX, radX, radZ, 0, true);
            ImagePlus filteredPlus = new ImagePlus("filtered_" + radX, res);
            timeDuplicate.setStack(res);
            filteredPlus.show();
        }
        IJ.log("Threshold method " + methods[threshold_method]);
        IJ.log("Criteria method " + criteria[crit]);
        int thmin = (int) minTh;
        // is starts at mean selected, use mean, maybe remove in new version
        if (start) {
            thmin = (int) ImageHandler.wrap(timeDuplicate).getMean();
            IJ.log("Mean=" + thmin);
        }

        TrackThreshold2 TT = new TrackThreshold2(volMin, volMax, minCont, step, step, thmin);

        // 8-bits switch to step method
        int tmethod = TrackThreshold2.THRESHOLD_METHOD_STEP;
        if (threshold_method == 0) {
            tmethod = TrackThreshold2.THRESHOLD_METHOD_STEP;
        } else if (threshold_method == 1) {
            tmethod = TrackThreshold2.THRESHOLD_METHOD_VOLUME;
        }
        if (timeDuplicate.getBitDepth() == 8) {
            threshold_method = TrackThreshold2.THRESHOLD_METHOD_STEP;
        }
        TT.setMethodThreshold(tmethod);
        int cri = TrackThreshold2.CRITERIA_METHOD_MIN_ELONGATION;
        switch (crit) {
            case 0:
                cri = TrackThreshold2.CRITERIA_METHOD_MIN_ELONGATION;
                break;
            case 1:
                cri = TrackThreshold2.CRITERIA_METHOD_MAX_COMPACTNESS;
                break;
            case 2:
                cri = TrackThreshold2.CRITERIA_METHOD_MAX_VOLUME;
                break;
            case 3:
                cri = TrackThreshold2.CRITERIA_METHOD_MSER;
                break;
            case 4:
                cri = TrackThreshold2.CRITERIA_METHOD_MAX_EDGES;
                break;
        }
        TT.setCriteriaMethod(cri);
        ImagePlus res;
        if (seg == 0)
            res = TT.segment(timeDuplicate, true);
        else
            res = TT.segmentBest(timeDuplicate, true);
        if ((res != null) && (calibration != null)) res.setCalibration(calibration);
        if (res != null) res.show();
        else IJ.log("NO OBJECTS FOUND !");
    }

    private boolean dialogue() {
        int nbima = WindowManager.getImageCount();
        String[] names = new String[nbima + 1];
        names[0] = "None";
        for (int i = 1; i <= nbima; i++) {
            names[i] = WindowManager.getImage(i).getShortTitle();
        }
        methods = new String[]{"STEP", "VOLUME"};
        criteria = new String[]{"ELONGATION", "COMPACTNESS", "VOLUME", "MSER", "EDGES"};
        segs = new String[]{"All", "Best"};
        GenericDialog gd = new GenericDialog("Iterative Thresholding 2");
        gd.addMessage("Image to segment : " + plus.getTitle());
        gd.addMessage("Only objects containing markers pixel will be detected.\nIf none all objects are detected.");
        gd.addChoice("Seeds", names, names[markers]);
        gd.addNumericField("Min_vol_pix", volMin, 0, 10, "");
        gd.addNumericField("Max_vol_pix", volMax, 0, 10, "");
        gd.addNumericField("Min_threshold", minTh, 0, 10, "");
        gd.addNumericField("Min_contrast (exp)", minCont, 0, 10, "");
        gd.addChoice("Criteria_method", criteria, criteria[crit]);
        gd.addChoice("Threshold_method", methods, methods[threshold_method]);
        gd.addChoice("Segment_results", segs, segs[seg]);
        gd.addNumericField("Value_method", step, 1, 10, "");
        gd.addCheckbox("Starts at mean", start);
        gd.addCheckbox("Filtering", filter);
        gd.showDialog();
        markers = gd.getNextChoiceIndex();
        volMin = (int) gd.getNextNumber();
        volMax = (int) gd.getNextNumber();
        minTh = (int) gd.getNextNumber();
        minCont = (int) gd.getNextNumber();
        crit = gd.getNextChoiceIndex();
        threshold_method = gd.getNextChoiceIndex();
        seg = gd.getNextChoiceIndex();
        step = (int) gd.getNextNumber();
        start = gd.getNextBoolean();
        filter = gd.getNextBoolean();

        if (volMax < volMin) {
            int vtemp = volMax;
            volMax = volMin;
            volMin = vtemp;
        }

        Prefs.set("mcib_iterative_volmax.int", volMax);
        Prefs.set("mcib_iterative_volmin.int", volMin);
        Prefs.set("mcib_iterative_thmin.int", minTh);
        Prefs.set("mcib_iterative_contmin.int", minCont);
        Prefs.set("mcib_iterative_method.int", threshold_method);
        Prefs.set("mcib_iterative_criteria.int", crit);
        Prefs.set("mcib_iterative_seg.int", seg);
        Prefs.set("mcib_iterative_step.int", step);

        return gd.wasOKed();
    }
}
