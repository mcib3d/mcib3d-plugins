package mcib_plugins.analysis;

import ij.IJ;
import ij.ImagePlus;
import ij.measure.ResultsTable;
import ij.plugin.Duplicator;
import mcib3d.geom.Objects3DPopulation;
import mcib3d.geom2.Objects3DIntPopulation;
import mcib3d.geom2.measurements.*;
import mcib3d.image3d.ImageHandler;
import mcib3d.image3d.ImageLabeller;

import java.util.List;

public class SimpleMeasure {
    public static final String CENTROID = "Centroid";
    public static final String DISTCENTER = "DistCenter";
    public static final String ELLIPSOID = "Ellipsoid";
    public static final String FERET = "Feret";
    public static final String SHAPE = "Shape";
    public static final String SURFACE = "Surface";
    public static final String VOLUME = "Volume";

    Objects3DIntPopulation population;
    ImageHandler input;
    ImagePlus inputPlus;

    public SimpleMeasure(ImagePlus in) {
        inputPlus = in;
        //input = ImageHandler.wrap(extractCurrentStack(inputPlus));
        input = ImageHandler.wrap(inputPlus);
        if (input.isBinary(0)) {
            IJ.log("Labelling image.");
            ImageLabeller label = new ImageLabeller();
            input = label.getLabels(input);
        }
        IJ.log("Computing population ...");
        population = new Objects3DIntPopulation(input);
    }

    public static ImagePlus extractCurrentStack(ImagePlus plus) {
        // check dimensions
        int[] dims = plus.getDimensions();//XYCZT
        int channel = plus.getChannel();
        int frame = plus.getFrame();
        ImagePlus stack;
        // crop actual frame
        if ((dims[2] > 1) || (dims[4] > 1)) {
            IJ.log("Hyperstack found, extracting current channel " + channel + " and frame " + frame);
            Duplicator duplicator = new Duplicator();
            stack = duplicator.run(plus, channel, channel, 1, dims[3], frame, frame);
            stack.setTitle(plus.getTitle() + "-C" + channel + "-T" + frame);
        } else stack = plus.duplicate();
        stack.setTitle(plus.getTitle());

        return stack;
    }

    public List<Double[]> getMeasuresCentroid() {
        return population.getMeasurementsList(new MeasureCentroid().getNamesMeasurement());
    }

    public List<Double[]> getMeasuresVolume() {
        return population.getMeasurementsList(new MeasureVolume().getNamesMeasurement());
    }

    public List<Double[]> getMeasuresSurface() {
        return population.getMeasurementsList(new MeasureSurface().getNamesMeasurement());
    }

    public List<Double[]> getMeasuresStats(ImageHandler raw) {
        return population.getMeasurementsIntensityList(new MeasureIntensity().getNamesMeasurement(), raw);
    }

    public List<Double[]> getMeasuresStatsHist(ImageHandler raw) {
        return population.getMeasurementsIntensityList(new MeasureIntensityHist().getNamesMeasurement(), raw);
    }

    public List<Double[]> getMeasuresStats(ImagePlus myPlus) {
        IJ.log("Computing measurements for " + population.getNbObjects() + " objects ...");
        return getMeasuresStats(ImageHandler.wrap(myPlus));
    }

    public List<Double[]> getMeasuresStatsHist(ImagePlus myPlus) {
        IJ.log("Computing measurements for " + population.getNbObjects() + " objects ...");
        return getMeasuresStatsHist(ImageHandler.wrap(myPlus));
    }

    public List<Double[]> getMeasuresCompactness() {
        return population.getMeasurementsList(new MeasureCompactness().getNamesMeasurement());
    }

    public List<Double[]> getMeasuresEllipsoid() {
        return population.getMeasurementsList(new MeasureEllipsoid().getNamesMeasurement());
    }

    public List<Double[]> getMeasuresDistanceCentreContour() {
        return population.getMeasurementsList(new MeasureDistancesCenter().getNamesMeasurement());
    }

    public List<Double[]> getMeasuresFeret() {
        return population.getMeasurementsList(new MeasureFeret().getNamesMeasurement());
    }

    @Deprecated
    public List<Double[]> getMeshSurfaces() {
        Objects3DPopulation pop = new Objects3DPopulation(input);
        return pop.getMeasuresMesh();
    }

    public ResultsTable getResultsTable(String measure) {
        IJ.log("Computing measurements for " + population.getNbObjects() + " objects ...");
        switch (measure) {
            case CENTROID:
                return getResultsTable(getMeasuresCentroid(), new MeasureCentroid().getNamesMeasurement());
            case DISTCENTER:
                return getResultsTable(getMeasuresDistanceCentreContour(), new MeasureDistancesCenter().getNamesMeasurement());
            case ELLIPSOID:
                return getResultsTable(getMeasuresEllipsoid(), new MeasureEllipsoid().getNamesMeasurement());
            case FERET:
                return getResultsTable(getMeasuresFeret(), new MeasureFeret().getNamesMeasurement());
            case SHAPE:
                return getResultsTable(getMeasuresCompactness(), new MeasureCompactness().getNamesMeasurement());
            case VOLUME:
                return getResultsTable(getMeasuresVolume(), new MeasureVolume().getNamesMeasurement());
            case SURFACE:
                return getResultsTable(getMeasuresSurface(), new MeasureSurface().getNamesMeasurement());
            default:
                return null;
        }
    }

    private ResultsTable getResultsTable(List<Double[]> res, String[] namesMeasurements) {
        String title = inputPlus.getTitle();
        int channel = inputPlus.getChannel();
        int frame = inputPlus.getFrame();
        int nbCT = inputPlus.getNChannels() * inputPlus.getNFrames();
        ResultsTable rt = ResultsTable.getResultsTable();
        if (rt == null) {
            rt = new ResultsTable();
        }
        int row = rt.getCounter();
        for (Double[] re : res) {
            rt.incrementCounter();
            for (int k = 0; k < namesMeasurements.length; k++) {
                if (re[k] == null) rt.setValue(namesMeasurements[k], row, Double.NaN);
                else rt.setValue(namesMeasurements[k], row, re[k]);
            }
            rt.setLabel(title, row);
            if (nbCT > 1) {
                rt.setValue("Channel", row, channel);
                rt.setValue("Frame", row, frame);
            }
            row++;
        }
        rt.sort("LabelObj");
        rt.updateResults();

        return rt;
    }
}
