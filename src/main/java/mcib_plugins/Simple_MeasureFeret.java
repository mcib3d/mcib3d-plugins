package mcib_plugins;

import ij.ImagePlus;
import ij.measure.ResultsTable;
import ij.plugin.filter.PlugInFilter;
import ij.process.ImageProcessor;
import mcib_plugins.analysis.SimpleMeasure;

public class Simple_MeasureFeret implements PlugInFilter {

    ImagePlus myPlus;
    String[] keysBase_s;

    @Override
    public int setup(String arg, ImagePlus imp) {
        myPlus = imp;
        return PlugInFilter.DOES_16 + PlugInFilter.DOES_8G + DOES_32;
    }

    @Override
    public void run(ImageProcessor ip) {
        SimpleMeasure mes = new SimpleMeasure(myPlus);
        ResultsTable rt = mes.getResultsTable(SimpleMeasure.FERET);
        rt.show("Results");
    }

}
