package mcib_plugins;

import ij.ImagePlus;
import ij.gui.GenericDialog;
import ij.plugin.filter.PlugInFilter;
import ij.process.AutoThresholder;
import ij.process.ImageProcessor;
import mcib3d.image3d.ImageHandler;
import mcib3d.image3d.segment.Segment3DNuclei;

public class SegNuclei_ implements PlugInFilter {
    private static final String[] methods = AutoThresholder.getMethods();
    ImagePlus myPlus;
    private String method = methods[0];
    private boolean separate = true;
    private float manual = 0;

    @Override
    public int setup(String s, ImagePlus imagePlus) {
        myPlus = imagePlus;
        return DOES_32 + DOES_16 + DOES_8G;
    }

    @Override
    public void run(ImageProcessor imageProcessor) {
        if (dialog()) {
            ImageHandler imageHandler = ImageHandler.wrap(myPlus);
            Segment3DNuclei segment3DNuclei = new Segment3DNuclei(imageHandler);
            segment3DNuclei.setMethod(AutoThresholder.Method.valueOf(method));
            segment3DNuclei.setManual(manual);
            segment3DNuclei.setSeparate(separate);
            ImageHandler seg = segment3DNuclei.segment();
            seg.show();
        }
    }

    private boolean dialog() {
        GenericDialog dialog = new GenericDialog("Nuclei Segmentation");
        dialog.addMessage("3D segmentation of fluorescent nuclei for cell cultures.");
        dialog.addChoice("Auto_Threshold", methods, methods[0]);
        dialog.addNumericField("Manual threshold (0=auto)", 0, 0, 6, null);
        dialog.addCheckbox("Separate_nuclei", separate);
        dialog.showDialog();
        method = dialog.getNextChoice();
        manual = (float) dialog.getNextNumber();
        separate = dialog.getNextBoolean();

        return dialog.wasOKed();
    }
}
