package mcib_plugins;

import ij.IJ;
import ij.ImagePlus;
import ij.Prefs;
import ij.WindowManager;
import ij.gui.GenericDialog;
import ij.plugin.PlugIn;
import mcib3d.geom2.Objects3DIntPopulation;
import mcib3d.geom2.measurementsPopulation.MeasurePopulationDistance;
import mcib3d.image3d.ImageHandler;

public class Distances_ implements PlugIn {
    private String measurement = MeasurePopulationDistance.DIST_CC_PIX;
    private double distMax = 1000;
    private int idxA, idxB;

    @Override
    public void run(String s) {
        int nbima = WindowManager.getImageCount();
        if (nbima < 1) {
            IJ.error("Needs at least one labelled image");
            return;
        }

        if (dialog()) {
            IJ.log("");
            IJ.log("Building objects population");
            ImagePlus plusA = WindowManager.getImage(idxA + 1);
            ImagePlus plusB = WindowManager.getImage(idxB + 1);
            Objects3DIntPopulation population1 = new Objects3DIntPopulation(ImageHandler.wrap(plusA));
            Objects3DIntPopulation population2 = new Objects3DIntPopulation(ImageHandler.wrap(plusB));

            IJ.log("Measuring distances");
            MeasurePopulationDistance distance = new MeasurePopulationDistance(population1, population2);
            distance.setDistanceMax(distMax);
            distance.setMeasurementMethod(measurement);
            distance.getResultsTableOnlyColoc().show(measurement);
        }
    }

    private boolean dialog() {
        int nbima = WindowManager.getImageCount();
        // choice for results table
        String[] namesA = new String[nbima];
        String[] namesB = new String[nbima];
        for (int i = 0; i < nbima; i++) {
            namesA[i] = WindowManager.getImage(i + 1).getShortTitle();
            namesB[i] = WindowManager.getImage(i + 1).getShortTitle();
        }
        // measurements
        String[] measurements = new String[4];
        measurements[0] = MeasurePopulationDistance.DIST_CC_PIX;
        measurements[1] = MeasurePopulationDistance.DIST_CC_UNIT;
        measurements[2] = MeasurePopulationDistance.DIST_BB_PIX;
        measurements[3] = MeasurePopulationDistance.DIST_BB_UNIT;
        idxA = 0;
        idxB = nbima > 1 ? 1 : 0;
        distMax = Prefs.get("3DSuiteDistances_distMax.double", distMax);

        GenericDialog dia = new GenericDialog("All Distances");
        dia.addChoice("Image_A", namesA, namesA[idxA]);
        dia.addChoice("Image_B", namesB, namesB[idxB]);
        dia.addChoice("Distance", measurements, measurement);
        dia.addNumericField("Distance_Maximum", distMax);

        dia.showDialog();

        if (dia.wasOKed()) {
            idxA = dia.getNextChoiceIndex();
            idxB = dia.getNextChoiceIndex();
            measurement = dia.getNextChoice();
            distMax = dia.getNextNumber();

            // Prefs
            Prefs.set("3DSuiteDistances_distMax.double", distMax);
        }

        return dia.wasOKed();
    }
}
