package mcib_plugins;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import ij.IJ;
import ij.ImagePlus;
import ij.WindowManager;
import ij.gui.GenericDialog;
import ij.plugin.filter.PlugInFilter;
import ij.process.ImageProcessor;
import mcib3d.geom2.Object3DComputation;
import mcib3d.geom2.Object3DInt;
import mcib3d.image3d.ImageByte;
import mcib3d.image3d.ImageHandler;
import mcib3d.image3d.ImageLabeller;
import mcib3d.image3d.ImageShort;

import java.time.Duration;
import java.time.Instant;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class Hysteresis_Thresholding implements PlugInFilter {
    ImagePlus plus;
    private int markers = 0;

    @Override
    public int setup(String arg, ImagePlus imp) {
        plus = imp;

        return DOES_8G + DOES_16 + DOES_32;
    }

    @Override
    public void run(ImageProcessor ip) {
        int nbima = WindowManager.getImageCount();
        String[] names = new String[nbima + 1];
        names[0] = "None";
        for (int i = 1; i <= nbima; i++) {
            names[i] = WindowManager.getImage(i).getShortTitle();
        }
        GenericDialog gd = new GenericDialog("Hysteresis Thresholding");
        gd.addMessage("Image to segment : " + plus.getTitle());
        gd.addMessage("Only objects containing markers pixel will be detected.\nIf none all objects are detected.");
        gd.addChoice("Seeds", names, names[markers]);
        gd.addNumericField("High Threshold:", 128, 1);
        gd.addNumericField("Low Threshold:", 50, 1);
        gd.addCheckbox("Show multi-threshold", false);
        gd.addCheckbox(" Labelling", true);
        gd.showDialog();
        if (gd.wasOKed()) {
            markers = gd.getNextChoiceIndex();
            double high = gd.getNextNumber();
            double low = gd.getNextNumber();
            boolean show = gd.getNextBoolean();
            boolean label = gd.getNextBoolean();

            Instant t0 = Instant.now();
            ImagePlus hyst = hysteresis(plus, low, high, show, label);
            Instant t1 = Instant.now();
            hyst.setDisplayRange(0, 255);
            hyst.show();
            IJ.log("Hysteresis took " + Duration.between(t0, t1));
        }
    }


    public ImagePlus hysteresis(ImagePlus image, double lowval, double highval, boolean show, boolean label) {
        int HIGH = 255;
        int LOW = 128;
        // first threshold the image
        ImageHandler img = ImageHandler.wrap(image);
        ImageByte multi = new ImageByte(image.getTitle() + "_Multi", img.sizeX, img.sizeY, img.sizeZ);
        for (int z = 0; z < img.sizeZ; z++) {
            for (int xy = 0; xy < img.sizeXY; xy++) {
                if (img.getPixel(xy, z) > highval) {
                    multi.setPixel(xy, z, HIGH);
                } else if (img.getPixel(xy, z) > lowval) {
                    multi.setPixel(xy, z, LOW);
                }
            }
        }
        if (show) multi.show();

        ImageHandler thresholded = multi.thresholdAboveInclusive(LOW);
        ImageLabeller labeller = new ImageLabeller();
        List<Object3DInt> objects = labeller.getObjects3D(thresholded);

        ImageHandler hyst;
        if (label) hyst = new ImageShort("HystLabel_" + image.getTitle(), multi.sizeX, multi.sizeY, multi.sizeZ);
        else hyst = new ImageByte("HystBin_" + image.getTitle(), multi.sizeX, multi.sizeY, multi.sizeZ);
        hyst.setVoxelSize(img);
        ImageHandler marker0 = null;
        if (markers > 0) marker0 = ImageHandler.wrap(WindowManager.getImage(markers));
        final ImageHandler marker = marker0;
        AtomicInteger val = new AtomicInteger(1);
        objects.stream().filter(obj -> new Object3DComputation(obj).hasOneVoxelValueRange(multi, HIGH, HIGH))
                .filter(obj -> {
                    if (marker == null) return true;
                    return new Object3DComputation(obj).hasOneVoxelValueAboveStrict(marker, 0);
                })
                .forEach(obj -> {
                    if (label) {
                        obj.drawObject(hyst, val.getAndIncrement());
                    } else {
                        obj.drawObject(hyst, 255);
                    }
                });

        return hyst.getImagePlus();
    }
}
