package mcib_plugins;

import ij.ImagePlus;
import ij.gui.GenericDialog;
import ij.plugin.filter.PlugInFilter;
import ij.process.ImageProcessor;
import mcib3d.geom2.Objects3DIntPopulation;
import mcib3d.geom2.Objects3DIntPopulationComputation;
import mcib3d.image3d.ImageFloat;
import mcib3d.image3d.ImageHandler;
import mcib3d.image3d.ImageShort;

/**
 * @author thomasb
 */
public class RemoveObjectsBorder_ implements PlugInFilter {

    ImagePlus plus;

    @Override
    public int setup(String string, ImagePlus ip) {
        plus = ip;

        return DOES_16 + DOES_32 + DOES_8G;
    }

    @Override
    public void run(ImageProcessor ip) {

        GenericDialog gd = new GenericDialog("Exclude borders");
        gd.addMessage("Remove objects on X-Y borders");
        gd.addCheckbox("Remove objects on Z border ?", false);
        gd.showDialog();
        if (gd.wasCanceled()) {
            return;
        }
        boolean Z = gd.getNextBoolean();


        ImageHandler handler = ImageHandler.wrap(plus);
        Objects3DIntPopulation population = new Objects3DIntPopulation(handler);
        Objects3DIntPopulation population1 = new Objects3DIntPopulationComputation(population).getExcludeBorders(handler, Z);

        ImageHandler handler1;

        if (plus.getBitDepth() > 16) {
            handler1 = new ImageFloat("Objects_removed", plus.getWidth(), plus.getHeight(), plus.getNSlices());
        } else {
            handler1 = new ImageShort("Objects_removed", plus.getWidth(), plus.getHeight(), plus.getNSlices());
        }

        population1.drawInImage(handler1);

        ImagePlus rem = handler1.getImagePlus();
        rem.setCalibration(plus.getCalibration());
        rem.show();
    }

}
