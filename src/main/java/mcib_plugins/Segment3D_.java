package mcib_plugins;

import ij.IJ;
import ij.ImagePlus;
import ij.WindowManager;
import ij.gui.GenericDialog;
import ij.plugin.filter.PlugInFilter;
import ij.process.ImageProcessor;
import mcib3d.image3d.ImageHandler;
import mcib3d.image3d.ImageInt;
import mcib3d.image3d.ImageLabeller;

public class Segment3D_ implements PlugInFilter {

    ImagePlus myPlus;
    // DB
    boolean debug = false;
    // idx of marker image
    int markers = 0;

    @Override
    public int setup(String arg, ImagePlus imp) {
        myPlus = imp;
        return DOES_8G + DOES_16 + DOES_32;
    }

    @Override
    public void run(ImageProcessor ip) {
        int low = 128;
        //int high = -1;
        int min = 0;
        int max = -1;
        boolean individual = false;
        boolean bits32 = false;// more than 65,535 objects
        int nbima = WindowManager.getImageCount();
        String[] names = new String[nbima + 1];
        names[0] = "None";
        for (int i = 1; i <= nbima; i++) {
            names[i] = WindowManager.getImage(i).getShortTitle();
        }
        GenericDialog gd = new GenericDialog("Segment3D");
        gd.addMessage("Image to segment : " + myPlus.getTitle());
        gd.addMessage("Only objects containing markers pixel will be detected.\nIf none all objects are detected.");
        gd.addChoice("Seeds", names, names[markers]);
        gd.addNumericField("Low_threshold (included)", low, 0);
        //gd.addNumericField("High_threshold (-1 for max)", high, 0);
        gd.addNumericField("Min_size", min, 0);
        gd.addNumericField("Max_size (-1 for infinity)", max, 0);
        gd.addCheckbox("Individual voxels are objects", individual);
        gd.addCheckbox("32-bit segmentation (nb objects > 65,535)", bits32);
        gd.showDialog();
        markers = gd.getNextChoiceIndex();
        low = (int) gd.getNextNumber();
        //high = (int) gd.getNextNumber();
        min = (int) gd.getNextNumber();
        max = (int) gd.getNextNumber();
        individual = gd.getNextBoolean();
        bits32 = gd.getNextBoolean();
        if (gd.wasCanceled()) {
            return;
        }
        ImageLabeller labeler = new ImageLabeller();
        if (min > 0) {
            labeler.setMinSize(min);
        }
        if (max > 0) {
            labeler.setMaxsize(max);
        }

        ImageHandler img = ImageHandler.wrap(myPlus);
        ImageInt bin = img.thresholdAboveInclusive(low);
        bin.setVoxelSize(img);
        bin.show("Bin");
        ImageHandler res;
        if (individual) res = labeler.getLabelsIndividualVoxels(bin);
        else {
            // no markers
            if (markers == 0) {
                if (bits32) res = labeler.getLabelsFloat(bin);
                else res = labeler.getLabels(bin);
                IJ.log("Nb obj total =" + labeler.getNbObjectsTotal(bin));
                IJ.log("Nb obj in range size =" + labeler.getNbObjectsInSizeRange(bin));
            } else { // markers
                ImageHandler marker = ImageHandler.wrap(WindowManager.getImage(markers));
                if (bits32) res = labeler.getLabelsWithSeedsFloat(bin, marker);
                else res = labeler.getLabelsWithSeeds(bin, marker);
            }
        }

        res.setVoxelSize(img);
        res.show("Seg");

    }
}
