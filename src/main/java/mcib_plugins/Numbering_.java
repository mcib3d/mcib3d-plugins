package mcib_plugins;

import ij.ImagePlus;
import ij.WindowManager;
import ij.gui.GenericDialog;
import ij.measure.ResultsTable;
import ij.plugin.filter.PlugInFilter;
import ij.process.ImageProcessor;
import mcib3d.geom2.Objects3DIntPopulation;
import mcib3d.geom2.measurements.MeasureNumbering;
import mcib3d.image3d.ImageHandler;
import mcib3d.image3d.ImagePlus_Utils;

import java.util.concurrent.atomic.AtomicInteger;

/*
 * To change this template, choose Tools | Templates and open the template in
 * the editor.
 */

/**
 * *
 * /**
 * Copyright (C) 2008- 2012 Thomas Boudier and others
 * <p>
 * <p>
 * <p>
 * This file is part of mcib3d
 * <p>
 * mcib3d is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3 of the License, or (at your option) any later
 * version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author thomas
 */
public class Numbering_ implements PlugInFilter {

    ImagePlus myPlus;
    int imaMain;
    int imaSignal;

    @Override
    public int setup(String arg, ImagePlus imp) {
        return PlugInFilter.DOES_16 + PlugInFilter.DOES_8G;
    }

    @Override
    public void run(ImageProcessor ip) {
        if (Dialogue()) {
            myPlus = WindowManager.getImage(imaMain);
            int channel = myPlus.getChannel();
            int frame = myPlus.getFrame();
            String title = myPlus.getTitle();
            ImageHandler img = ImageHandler.wrap(ImagePlus_Utils.extractCurrentStack(myPlus));

            myPlus = WindowManager.getImage(imaSignal);
            ImageHandler spots = ImageHandler.wrap(ImagePlus_Utils.extractCurrentStack(myPlus));

            ResultsTable rt = ResultsTable.getResultsTable();
            if (rt == null) {
                rt = new ResultsTable();
            }
            rt.reset();
            final int r0 = rt.getCounter();
            final ResultsTable rt2 = rt;

            // numbering V2
            Objects3DIntPopulation pop = new Objects3DIntPopulation(img);
            AtomicInteger row = new AtomicInteger(r0);
            pop.getObjects3DInt().forEach(O -> {
                MeasureNumbering numbering = new MeasureNumbering(O, spots);
                rt2.incrementCounter();
                rt2.setValue("Value", row.get(), O.getLabel());
                rt2.setValue("NbObjects", row.get(), numbering.getValueMeasurement(MeasureNumbering.OBJ_NUMBER));
                rt2.setValue("VolObjects", row.get(), numbering.getValueMeasurement(MeasureNumbering.OBJ_VOLUME));
                rt2.setValue("PercObjects", row.get(), numbering.getValueMeasurement(MeasureNumbering.OBJ_PERCENT));
                rt2.setValue("Channel", row.get(), channel);
                rt2.setValue("Frame", row.get(), frame);
                rt2.setValue("Label", row.get(), title);
                row.getAndIncrement();
            });
            rt.sort("Value");
            rt.show("Numbering");
        }
    }

    private boolean Dialogue() {
        int nbima = WindowManager.getImageCount();
        String[] names = new String[nbima];
        for (int i = 0; i < nbima; i++) {
            names[i] = WindowManager.getImage(i + 1).getShortTitle();
        }
        imaMain = 0;
        imaSignal = nbima > 1 ? nbima - 1 : 0;

        GenericDialog dia = new GenericDialog("Objects numbering");
        dia.addChoice("Main objects(containing)", names, names[imaMain]);
        dia.addChoice("Counted objects inside", names, names[imaSignal]);
        dia.showDialog();
        imaMain = dia.getNextChoiceIndex() + 1;
        imaSignal = dia.getNextChoiceIndex() + 1;

        return dia.wasOKed();
    }
}
