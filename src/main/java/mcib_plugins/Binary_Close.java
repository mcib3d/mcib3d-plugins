/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mcib_plugins;

import ij.IJ;
import ij.ImagePlus;
import ij.gui.GenericDialog;
import ij.measure.Calibration;
import ij.plugin.filter.PlugInFilter;
import ij.process.ImageProcessor;
import mcib3d.image3d.ImageHandler;
import mcib3d.image3d.processing.BinaryMultiLabel;

public class Binary_Close implements PlugInFilter {
    float radiusXY, radiusZ;
    String method = "Close";
    ImagePlus plus;

    public ImageHandler process(ImageHandler input) {
        float radXY = Math.max(radiusXY, 1);
        float radZ = Math.max(radiusZ, 0);
        if (input.sizeZ == 1) radZ = 0;
        switch (method) {
            case "Open":
                return BinaryMultiLabel.binaryOpenMultilabel(input, radXY, radZ);
            case "Dilate":
                return BinaryMultiLabel.binaryDilateMultilabel(input, radXY, radZ);
            case "Erode":
                return BinaryMultiLabel.binaryErodeMultiLabel(input, radXY, radZ);
            case "FillHoles":
                return BinaryMultiLabel.fillHoles2DMultiLabel(input);
            default:
                return BinaryMultiLabel.binaryCloseMultilabel(input, radXY, radZ);
        }
    }

    @Override
    public int setup(String arg, ImagePlus imp) {
        plus = imp;
        return PlugInFilter.DOES_8G + PlugInFilter.DOES_16 + DOES_32;
    }

    @Override
    public void run(ImageProcessor ip) {
        IJ.showStatus("BinaryClose");
        GenericDialog gd = new GenericDialog("BinaryClose");
        gd.addNumericField("radiusXY (pix):", 5, 1);
        gd.addNumericField("radiusZ (pix):", 3, 1);
        gd.addChoice("Operation", new String[]{"Close", "Dilate", "Open", "Erode", "FillHoles"}, method);
        gd.showDialog();
        ImageHandler input = ImageHandler.wrap(plus);
        if (gd.wasOKed()) {
            radiusXY = (float) gd.getNextNumber();
            radiusZ = (float) gd.getNextNumber();
            method = gd.getNextChoice();
            ImageHandler res = process(input);
            ImagePlus resPlus = res.getImagePlus();
            Calibration cal = plus.getCalibration();
            if (cal != null) {
                resPlus.setCalibration(cal);
            }
            resPlus.setTitle("CloseLabels");
            resPlus.show();
        }
    }
}
