package mcib_plugins;

import ij.IJ;
import ij.ImagePlus;
import ij.WindowManager;
import ij.gui.GenericDialog;
import ij.plugin.PlugIn;
import mcib3d.image3d.ImageHandler;
import mcib3d.image3d.processing.ImageStitcher;

import java.util.LinkedList;
import java.util.List;

public class Stitch2_ implements PlugIn {
    private final static int UNION = 1;
    private final static int INTERSECTION = 2;
    private static int mergeMethod = 1;
    int ima0 = 0;
    int ima1 = 1;
    private double minRatioColoc = 0.5;

    @Override
    public void run(String s) {
        if (WindowManager.getImageCount() < 2) {
            IJ.showMessage("Requires two images");
            return;
        }
        if (Dialogue()) {
            ImagePlus plus0 = WindowManager.getImage(ima0);
            ImagePlus plus1 = WindowManager.getImage(ima1);
            ImageHandler handler0 = ImageHandler.wrap(plus0);
            ImageHandler handler1 = ImageHandler.wrap(plus1);
            List<ImageHandler> images = new LinkedList<>();
            images.add(handler0);
            images.add(handler1);
            // do the stitching
            ImageHandler draw = handler0.createSameDimensions();
            ImageStitcher stitcher = new ImageStitcher(images);
            int merge = mergeMethod == 0 ? UNION : INTERSECTION;
            stitcher.setMergeMethod(merge);
            stitcher.stitchLabelImages(minRatioColoc).drawInImage(draw);
            draw.show(handler0.getTitle() + "_" + handler1.getTitle());
        }
    }

    private boolean Dialogue() {
        int nbima = WindowManager.getImageCount();
        String[] names = new String[nbima];
        for (int i = 0; i < nbima; i++) {
            names[i] = WindowManager.getImage(i + 1).getShortTitle();
        }

        String[] methods = new String[]{"Union", "Intersection"};
        GenericDialog dia = new GenericDialog("Stitch label images");
        dia.addChoice("ImageA", names, names[ima0]);
        dia.addChoice("ImageB", names, names[ima1]);
        dia.addNumericField("Min ratio Coloc", minRatioColoc);
        dia.addChoice("Merge method", methods, methods[mergeMethod]);
        dia.showDialog();
        ima0 = dia.getNextChoiceIndex() + 1;
        ima1 = dia.getNextChoiceIndex() + 1;
        minRatioColoc = dia.getNextNumber();
        mergeMethod = dia.getNextChoiceIndex();

        return dia.wasOKed();
    }
}
