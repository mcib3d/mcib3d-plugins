package mcib_plugins;

import ij.ImagePlus;
import ij.WindowManager;
import ij.gui.GenericDialog;
import ij.measure.ResultsTable;
import ij.plugin.filter.PlugInFilter;
import ij.process.ImageProcessor;
import mcib3d.geom2.measurements.MeasureIntensity;
import mcib3d.geom2.measurements.MeasureIntensityHist;
import mcib3d.image3d.ImageHandler;
import mcib_plugins.analysis.SimpleMeasure;

import java.util.List;

public class Simple_MeasureStatistics implements PlugInFilter {

    ImagePlus myPlus;
    int imaSpots;
    int imaSignal;

    @Override
    public int setup(String arg, ImagePlus imp) {
        return PlugInFilter.DOES_16 + PlugInFilter.DOES_8G + DOES_32;
    }

    @Override
    public void run(ImageProcessor ip) {
        if (Dialogue()) {
            myPlus = WindowManager.getImage(imaSpots);
            int channel = myPlus.getChannel();
            int frame = myPlus.getFrame();
            int nbCT = myPlus.getNChannels() * myPlus.getNFrames();
            String title = myPlus.getTitle();
            ImageHandler img = ImageHandler.wrap(SimpleMeasure.extractCurrentStack(myPlus));
            ImagePlus seg = img.getImagePlus();

            SimpleMeasure mes = new SimpleMeasure(seg);
            ResultsTable rt = ResultsTable.getResultsTable();
            if (rt == null) {
                rt = new ResultsTable();
            }
            myPlus = WindowManager.getImage(imaSignal);
            ImagePlus plusSignal = SimpleMeasure.extractCurrentStack(myPlus);
            title = title.concat(":");
            title = title.concat(plusSignal.getTitle());
            // Intensity
            List<Double[]> res = mes.getMeasuresStats(plusSignal);
            int row = rt.getCounter();
            int row0 = row;
            String[] keys_s = new MeasureIntensity().getNamesMeasurement();
            for (Double[] re : res) {
                rt.incrementCounter();
                for (int k = 0; k < keys_s.length; k++) {
                    if (re[k] == null) {
                        rt.setValue(keys_s[k], row, Double.NaN);
                    } else {
                        rt.setValue(keys_s[k], row, re[k]);
                    }
                }
                rt.setLabel(title, row);
                row++;
            }
            // Intensity Hist
            res = mes.getMeasuresStatsHist(plusSignal);
            row = row0;
            String[] keys_s1 = new MeasureIntensityHist().getNamesMeasurement();
            for (Double[] re : res) {
                for (int k = 0; k < keys_s1.length; k++) {
                    rt.setValue(keys_s1[k], row, re[k]);
                }
                if (nbCT > 1) {
                    rt.setValue("Channel", row, channel);
                    rt.setValue("Frame", row, frame);
                }
                row++;
            }
            rt.sort("LabelObj");
            rt.updateResults();
            rt.show("Results");
        }
    }

    private boolean Dialogue() {
        int nbima = WindowManager.getImageCount();
        String[] names = new String[nbima];
        for (int i = 0; i < nbima; i++) {
            names[i] = WindowManager.getImage(i + 1).getShortTitle();
        }
        imaSpots = 0;
        imaSignal = nbima > 1 ? nbima - 1 : 0;

        GenericDialog dia = new GenericDialog("Statistical measure");
        dia.addChoice("Objects", names, names[imaSpots]);
        dia.addChoice("Signal", names, names[imaSignal]);
        dia.showDialog();
        imaSpots = dia.getNextChoiceIndex() + 1;
        imaSignal = dia.getNextChoiceIndex() + 1;

        return dia.wasOKed();
    }
}
