package mcib_plugins.tools;

import ij.IJ;
import mcib3d.geom.Point3D;
import mcib3d.geom.Vector3D;
import mcib3d.geom2.BoundingBox;
import mcib3d.image3d.ImageHandler;
import mcib3d.utils.ThreadUtil;
import org.scijava.vecmath.Point3f;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class Voxelizer {
    List<Point3f> mesh;

    public Voxelizer(List<Point3f> mesh) {
        this.mesh = mesh;
    }

    public static BoundingBox boundingBox(List<Point3f> mesh) {
        float xmin = Float.POSITIVE_INFINITY, xmax = Float.NEGATIVE_INFINITY;
        float ymin = Float.POSITIVE_INFINITY, ymax = Float.NEGATIVE_INFINITY;
        float zmin = Float.POSITIVE_INFINITY, zmax = Float.NEGATIVE_INFINITY;
        for (Point3f P : mesh) {
            xmin = Math.min(xmin, P.x);
            ymin = Math.min(ymin, P.y);
            zmin = Math.min(zmin, P.z);
            xmax = Math.max(xmax, P.x);
            ymax = Math.max(ymax, P.y);
            zmax = Math.max(zmax, P.z);
        }

        return new BoundingBox((int) Math.floor(xmin), (int) Math.ceil(xmax), (int) Math.floor(ymin), (int) Math.ceil(ymax), (int) Math.floor(zmin), (int) Math.ceil(zmax));
    }

    public void voxelize(ImageHandler draw, boolean verbose) {
        BoundingBox finalBox = boundingBox(mesh);

        int nCpus = ThreadUtil.getNbCpus();
        final AtomicInteger ai = new AtomicInteger(0);
        final AtomicInteger countZ = new AtomicInteger(0);
        Thread[] threads = ThreadUtil.createThreadArray(nCpus);
        int deltaZ = finalBox.zmax - finalBox.zmin + 1;
        final int dec = (int) Math.ceil((double) (deltaZ) / (double) nCpus);
        for (int ithread = 0; ithread < threads.length; ithread++) {
            threads[ithread] = new Thread(() -> {
                for (int k = ai.getAndIncrement(); k < nCpus; k = ai.getAndIncrement()) {
                    int zmin = finalBox.zmin + dec * k;
                    int zmax = Math.min(finalBox.zmin + (k + 1) * dec, finalBox.zmax);
                    for (int z = zmin; z < zmax; z++) {
                        if (verbose)
                            IJ.log("\\Update:Processing " + countZ.getAndIncrement() + "/" + deltaZ + "      ");
                        for (int y = finalBox.ymin; y <= finalBox.ymax; y++) {
                            processX(mesh, draw, y, z);
                        }
                    }
                }
            });
        }
        try {
            ThreadUtil.startAndJoin(threads);
        } catch (RuntimeException r) {
            System.out.println("Pb " + r);
        }
    }

    private Point3D projectionLineTriangle(Point3D p1, Point3D p2, Point3D p3, Point3D m, Vector3D L) {
        // q1 and q2 two point on the line very far
        Vector3D LL = L.getNormalizedVector();
        double big = 65535;
        Point3D q1 = new Point3D(m.x + big * LL.x, m.y + big * LL.y, m.z + big * LL.z);
        Point3D q2 = new Point3D(m.x - big * LL.x, m.y - big * LL.y, m.z - big * LL.z);
        // test signed volumes
        double SVq1p1p2p3 = signVolumeTetra(q1, p1, p2, p3);
        double SVq2p1p2p3 = signVolumeTetra(q2, p1, p2, p3);
        if (SVq1p1p2p3 * SVq2p1p2p3 > 0) return null; // should have different sign
        // next rest
        double sv1 = signVolumeTetra(q1, q2, p1, p2);
        double sv2 = signVolumeTetra(q1, q2, p2, p3);
        double sv3 = signVolumeTetra(q1, q2, p3, p1);
        // all pos or all neg
        if (((sv1 >= 0) && (sv2 >= 0) && (sv3 >= 0)) || ((sv1 <= 0) && (sv2 <= 0) && (sv3 <= 0))) {
            // compute
            Vector3D P1Q1 = new Vector3D(p1, q1);
            Vector3D Q1Q2 = new Vector3D(q1, q2);
            Vector3D N = new Vector3D(p1, p2).crossProduct(new Vector3D(p1, p3));
            double den = Q1Q2.dotProduct(N);
            double t;
            if (den != 0) t = -P1Q1.dotProduct(N) / den;
            else t = P1Q1.getLength();
            //System.out.println("PROJ " + N + " " + t);
            return new Vector3D(q1.x + t * Q1Q2.x, q1.y + t * Q1Q2.y, q1.z + t * Q1Q2.z);
        } else return null;
    }

    private double signVolumeTetra(Point3D a, Point3D b, Point3D c, Point3D d) {
        // (1.0/6.0)*dot(cross(b-a,c-a),d-a)
        Vector3D AB = new Vector3D(a, b);
        Vector3D AC = new Vector3D(a, c);
        Vector3D AD = new Vector3D(a, d);

        return AB.crossProduct(AC).dotProduct(AD) / 6.0;
    }

    private void processX(List<Point3f> mesh, ImageHandler draw, int y, int z) {
        Vector3D dir = new Vector3D(1, 0, 0);
        Point3D m = new Point3D(0, y, z);
        Point3D A, B, C, P;
        List<Point3D> projs = new ArrayList<>();
        for (int i = 0; i < mesh.size(); i += 3) {
            Point3f p = mesh.get(i);
            A = new Point3D(p.x, p.y, p.z);
            p = mesh.get(i + 1);
            B = new Point3D(p.x, p.y, p.z);
            p = mesh.get(i + 2);
            C = new Point3D(p.x, p.y, p.z);
            P = projectionLineTriangle(A, B, C, m, dir);
            if (P != null) projs.add((P));
        }
        if (!projs.isEmpty()) {
            Point3D finalM = m;
            projs.sort((p1, p2) -> (int) Math.signum(finalM.distanceSquare(p1) - finalM.distanceSquare(p2)));
            //projs.forEach(System.out::println);
            boolean in = false;
            int idx = 0;
            int idxx = findNextPoint(projs, 0);
            while (idxx >= 0) {
                if (!in) {
                    drawLine(draw, (int) projs.get(idx).x, (int) projs.get(idxx).x, (int) projs.get(idx).y, (int) projs.get(idx).z);
                    in = true;
                } else in = false;
                idx = idxx;
                idxx = findNextPoint(projs, idx);
            }
        }
    }

    private void drawLine(ImageHandler draw, int x0, int x1, int y0, int z0) {
        //System.out.println("Line " + x0 + " " + x1 + " " + y0 + " " + z0);
        for (int x = x0; x <= x1; x++) {
            if (draw.contains(x, y0, z0)) draw.setPixel(x, y0, z0, 255);
        }
    }

    private int findNextPoint(List<Point3D> projs, int idx) {
        Point3D P0 = projs.get(idx);
        // skip duplicate points
        double epsilon = 0.000001;
        while ((idx < projs.size() - 1) && (P0.distanceSquare(projs.get(idx + 1)) < epsilon)) {
            idx++;
        }
        if (idx < projs.size() - 1) {
            return idx + 1;
        } else return -1;
    }

}
