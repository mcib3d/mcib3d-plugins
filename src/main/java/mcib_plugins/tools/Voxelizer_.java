package mcib_plugins.tools;

import customnode.CustomMesh;
import customnode.MeshLoader;
import ij.IJ;
import ij.gui.GenericDialog;
import ij.plugin.PlugIn;
import mcib3d.geom2.BoundingBox;
import mcib3d.image3d.ImageHandler;
import mcib3d.image3d.ImageShort;
import org.scijava.vecmath.Point3f;

import java.util.List;
import java.util.Map;

public class Voxelizer_ implements PlugIn {
    boolean scaleToImage = false;
    String file = "";
    private int imaX = 256;
    private int imaY = 256;
    private int imaZ = 256;

    @Override
    public void run(String s) {
        if (dialog()) {
            IJ.log("Loading mesh file " + file);
            Map<String, CustomMesh> meshMap = MeshLoader.loadSTL(file);
            ImageHandler draw = new ImageShort("voxels", imaX, imaY, imaZ);
            for (String me : meshMap.keySet()) {
                List<Point3f> mesh = meshMap.get(me).getMesh();
                BoundingBox box = Voxelizer.boundingBox(mesh);
                // translate mesh
                if (box.xmin < 0) for (Point3f point3f : mesh) {
                    point3f.x -= box.xmin;
                }
                if (box.ymin < 0) for (Point3f point3f : mesh) {
                    point3f.y -= box.ymin;
                }
                if (box.zmin < 0) for (Point3f point3f : mesh) {
                    point3f.z -= box.zmin;
                }
                box = Voxelizer.boundingBox(mesh);
                // scale mesh
                float scaleX = (float) draw.sizeX / (float) box.xmax;
                float scaleY = (float) draw.sizeY / (float) box.ymax;
                float scaleZ = (float) draw.sizeZ / (float) box.zmax;
                float scale = Math.min(scaleX, Math.min(scaleY, scaleZ));
                if (scaleToImage) {
                    for (Point3f point3f : mesh) {
                        point3f.x *= scale;
                        point3f.y *= scale;
                        point3f.z *= scale;
                    }
                }
                // voxelization
                new Voxelizer(mesh).voxelize(draw, true);

                draw.show("Voxels");
                System.out.println("DONE!");
            }
        }
    }

    private boolean dialog() {
        GenericDialog dialog = new GenericDialog("Voxelizer");
        dialog.addFileField("Choose mesh file", IJ.getDirectory("home"));
        dialog.addNumericField("Image_sizeX", imaX);
        dialog.addNumericField("Image_sizeY", imaY);
        dialog.addNumericField("Image_sizeZ", imaZ);
        dialog.addCheckbox("Scale to image", scaleToImage);
        dialog.showDialog();
        file = dialog.getNextString();
        imaX = (int) dialog.getNextNumber();
        imaY = (int) dialog.getNextNumber();
        imaZ = (int) dialog.getNextNumber();
        scaleToImage = dialog.getNextBoolean();

        return dialog.wasOKed();
    }


}
