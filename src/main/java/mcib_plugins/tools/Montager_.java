package mcib_plugins.tools;

import ij.IJ;
import ij.ImagePlus;
import ij.WindowManager;
import ij.gui.GenericDialog;
import ij.plugin.PlugIn;
import ij.plugin.filter.ThresholdToSelection;
import ij.plugin.frame.RoiManager;
import ij.process.ImageProcessor;
import mcib3d.geom2.BoundingBox;
import mcib3d.geom2.Objects3DIntPopulation;
import mcib3d.geom2.measurements.MeasureObject;
import mcib3d.geom2.measurements.MeasureVolume;
import mcib3d.image3d.ImageHandler;
import mcib3d.image3d.ImageShort;
import mcib3d.image3d.processing.ImageMontager;
import mcib3d.image3d.processing.ImageThumbnailer;

import java.text.NumberFormat;
import java.util.concurrent.atomic.AtomicBoolean;

public class Montager_ implements PlugIn {
    @Override
    public void run(String s) {
        int nbIma = WindowManager.getImageCount();

        if (nbIma < 1) {
            IJ.showMessage("No image opened !");
            return;
        }

        String[] namesRaw = new String[nbIma];
        String[] namesSeg = new String[nbIma];

        for (int i = 0; i < nbIma; i++) {
            namesRaw[i] = WindowManager.getImage(i + 1).getShortTitle();
            namesSeg[i] = WindowManager.getImage(i + 1).getShortTitle();
        }

        int seg = 0;
        int raw = nbIma > 1 ? 1 : 0;
        boolean descending = true;
        MeasureObject measureObject = new MeasureObject();
        String[] measures = measureObject.listMeasures().stream().sorted(String::compareTo).toArray(String[]::new);
        GenericDialog dia = new GenericDialog("Montager");
        int sizeX = 1000, sizeY = 1000;
        dia.addMessage("Images");
        dia.addChoice("Raw", namesRaw, namesRaw[raw]);
        dia.addChoice("Seg", namesSeg, namesSeg[seg]);
        dia.addMessage("Measurement 3D");
        dia.addChoice("Measure", measures, MeasureVolume.VOLUME_PIX);
        dia.addCheckbox("Descending order", descending);
        dia.addMessage("Display");
        dia.addNumericField("Width of montage", sizeX, 0);
        dia.addNumericField("Height maximum of montage", sizeY, 0);
        dia.addChoice("Thumb size", new String[]{"scaled", "fixed width", "fixed height"}, "scaled");
        dia.addNumericField("Value (scale or fixed)", 1, 2);
        dia.showDialog();

        if (dia.wasOKed()) {
            raw = dia.getNextChoiceIndex();
            seg = dia.getNextChoiceIndex();
            descending = dia.getNextBoolean();
            int mes = dia.getNextChoiceIndex();
            sizeX = (int) dia.getNextNumber();
            sizeY = (int) dia.getNextNumber();
            int scaledFix = dia.getNextChoiceIndex();
            double display = dia.getNextNumber();

            ImagePlus segPlus = WindowManager.getImage(seg + 1);
            ImagePlus rawPlus = WindowManager.getImage(raw + 1);
            final ImageHandler rawImg = ImageHandler.wrap(rawPlus);

            int desc = descending ? -1 : 1;
            final ImageMontager montagerRaw = new ImageMontager(sizeX, sizeY);
            final ImageMontager montagerObj = new ImageMontager(sizeX, sizeY);
            Objects3DIntPopulation population = new Objects3DIntPopulation(ImageHandler.wrap(segPlus));
            // perform measurement
            population.getObjects3DInt().forEach(obj -> {
                MeasureObject measure = new MeasureObject(obj);
                obj.setCompareValue(measure.measure(measures[mes]));
            });
            final AtomicBoolean atomicBoolean = new AtomicBoolean(true);
            NumberFormat nf = NumberFormat.getInstance();
            nf.setGroupingUsed(false);
            nf.setMaximumFractionDigits(0);
            population.getObjects3DInt().stream()
                    .sorted((o1, o2) -> (int) (desc * (o1.getCompareValue() - o2.getCompareValue())))
                    .forEach(obj -> {
                        if (atomicBoolean.get()) {
                            BoundingBox box = obj.getBoundingBox();
                            // thumbnail raw
                            ImageHandler rawCrop = rawImg.crop3D("crop", box.xmin, box.xmax, box.ymin, box.ymax, box.zmin, box.zmax);
                            ImageThumbnailer thumbnailerRaw = new ImageThumbnailer(rawCrop);
                            // thumb object
                            int sx = (int) (Math.ceil(box.xmax - box.xmin + 1));
                            int sy = (int) (Math.ceil(box.ymax - box.ymin + 1));
                            int sz = (int) (Math.ceil(box.zmax - box.zmin + 1));
                            ImageHandler thumbObj = new ImageShort("test", sx, sy, sz);
                            obj.drawObjectTranslate(thumbObj, -box.xmin, -box.ymin, -box.zmin, obj.getLabel());
                            ImageThumbnailer thumbnailerObj = new ImageThumbnailer(thumbObj);
                            // resize
                            ImagePlus plusRaw, plusObj;
                            if (scaledFix == 0) {
                                plusRaw = thumbnailerRaw.getThumbnailHeight((int) ((box.ymax - box.ymin + 1) * display));
                                plusObj = thumbnailerObj.getThumbnailHeight((int) ((box.ymax - box.ymin + 1) * display));
                            } else {
                                if (scaledFix == 1) {
                                    plusRaw = thumbnailerRaw.getThumbnailWidth((int) display);
                                    plusObj = thumbnailerObj.getThumbnailWidth((int) display);
                                } else {
                                    plusRaw = thumbnailerRaw.getThumbnailHeight((int) display);
                                    plusObj = thumbnailerObj.getThumbnailHeight((int) display);
                                }
                            }
                            atomicBoolean.set(montagerRaw.addImage(plusRaw));
                            montagerObj.addImage(plusObj);
                        }
                    });
            montagerRaw.getMontage().show();
            montagerObj.getMontage().show();
            // ROI
            ImagePlus plus = montagerObj.getMontage().duplicate();
            plus.getProcessor().setThreshold(1, 65535, ImageProcessor.NO_LUT_UPDATE);
            ThresholdToSelection toSelection = new ThresholdToSelection();
            toSelection.setup("", plus);
            toSelection.run(plus.getProcessor());
            RoiManager manager = RoiManager.getInstance();
            if (manager == null) manager = new RoiManager();
            manager.addRoi(plus.getRoi());
            manager.setVisible(true);
        }
    }
}
