package mcib_plugins;

import ij.IJ;
import ij.ImagePlus;
import ij.ImageStack;
import ij.WindowManager;
import ij.gui.GenericDialog;
import ij.gui.Toolbar;
import ij.measure.Calibration;
import ij.plugin.PlugIn;
import ij3d.Content;
import ij3d.Image3DUniverse;
import mcib3d.geom.GeomTransform3D;
import mcib3d.geom.Object3DSurface;
import mcib3d.geom.ObjectCreator3D;
import mcib3d.geom.Vector3D;
import org.scijava.vecmath.Color3f;

import java.awt.*;
import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

public class Shape3D_ implements PlugIn {

    @Override
    public void run(String arg) {
        int tx = 512;
        int ty = 512;
        int tz = 512;
        double cx = tx / 2.0;
        double cy = ty / 2.0;
        double cz = tz / 2.0;
        double rx = tx / 8.0;
        double ry = ty / 8.0;
        double rz = tz / 8.0;
        double vx = 1;
        double vy = 0;
        double vz = 0;
        double wx = 0;
        double wy = 1;
        double wz = 0;
        //boolean sphere = true;

        double resXY = 1.0;
        double resZ = 1.0;
        String unit = "pix";

        // 3D VIEWER
        List<Image3DUniverse> Viewers3D = Image3DUniverse.universes;
        Image3DUniverse universe;
        boolean viewer3d;
        if (Viewers3D.size() > 0) {
            viewer3d = true;
            universe = Viewers3D.get(0);
        } else {
            viewer3d = false;
            universe = null;
        }

        boolean gauss = false;

        //boolean overwrite = false;
        String title;
        Calibration cal = null;
        ImagePlus plus = WindowManager.getCurrentImage();
        if (plus != null) {
            title = plus.getShortTitle();
        } else {
            title = "*None*";
        }
        ImageStack stack = null;
        if (WindowManager.getWindowCount() > 0) {
            stack = plus.getStack();
            tx = stack.getWidth();
            ty = stack.getHeight();
            tz = stack.getSize();
            cx = tx / 2.0;
            cy = ty / 2.0;
            cz = tz / 2.0;
            rx = tx / 8.0;
            ry = ty / 8.0;
            rz = tz / 8.0;
            // calibration, fix thanks to Giovanni Cardone
            cal = WindowManager.getCurrentImage().getCalibration();
            if (cal != null) {
                if (cal.scaled()) {
                    resXY = cal.pixelWidth;
                    resZ = cal.pixelDepth;
                    unit = cal.getUnits();
                    cx = cal.getX(cx);
                    cy = cal.getY(cy);
                    cz = cal.getZ(cz);
                    rx = cal.getX(rx);
                    ry = cal.getY(ry);
                    rz = cal.getZ(rz);
                }
            }
        }
        if (cal == null) {
            cal = new Calibration();
            cal.pixelWidth = resXY;
            cal.pixelHeight = resXY;
            cal.pixelDepth = resZ;
            cal.setUnit(unit);
            unit = "pix";
        }
        NumberFormat nf = NumberFormat.getInstance(Locale.ENGLISH);
        nf.setMaximumFractionDigits(3);
        String[] displays = {"New stack", "Overwrite", "None"};
        int display = 0;
        GenericDialog gd = new GenericDialog("Sphere");
        gd.addMessage("Image: ", new Font("Arial", Font.BOLD, 12));
        gd.addMessage("Size in pixel");
        gd.addStringField("Size sx,sy,sz", tx + "," + ty + "," + tz, 25);
        gd.addMessage("Calibration (for new stack): ", new Font("Arial", Font.BOLD, 12));
        gd.addNumericField("Res_XY", resXY, 3);
        gd.addNumericField("Res_Z", resZ, 3);
        gd.addStringField("Unit", unit);
        gd.addMessage("Shape: ", new Font("Arial", Font.BOLD, 12));
        gd.addMessage("Centre in " + unit);
        gd.addStringField("Centre cx,cy,cz", nf.format(cx) + "," + nf.format(cy) + "," + nf.format(cz), 25);
        gd.addMessage("Radius in " + unit);
        gd.addStringField("Radius rx,ry,rz", nf.format(rx) + "," + nf.format(ry) + "," + nf.format(rz), 25);
        gd.addMessage("Orientation: ", new Font("Arial", Font.BOLD, 12));
        gd.addStringField("Vector1 vx,vy,vz", vx + "," + vy + "," + vz, 25);
        gd.addStringField("Vector2 wx,wy,wz", wx + "," + wy + "," + wz, 25);
        gd.addMessage("Display: ", new Font("Arial", Font.BOLD, 12));
        gd.addNumericField("Value", 255, 0);
        gd.addCheckbox("Gradient", gauss);
        gd.addChoice("Display", displays, displays[display]);
        gd.addCheckbox("3D_viewer", viewer3d);
        gd.showDialog();
        if (gd.wasOKed()) {
            String res = gd.getNextString();
            String[] vals = res.split(",");
            tx = Integer.parseInt(vals[0].trim());
            ty = Integer.parseInt(vals[1].trim());
            tz = Integer.parseInt(vals[2].trim());
            resXY = gd.getNextNumber();
            resZ = gd.getNextNumber();
            unit = gd.getNextString();
            res = gd.getNextString();
            vals = res.split(",");
            cx = Double.parseDouble(vals[0].trim());
            cy = Double.parseDouble(vals[1].trim());
            cz = Double.parseDouble(vals[2].trim());
            res = gd.getNextString();
            vals = res.split(",");
            rx = Double.parseDouble(vals[0].trim());
            ry = Double.parseDouble(vals[1].trim());
            rz = Double.parseDouble(vals[2].trim());
            res = gd.getNextString();
            vals = res.split(",");
            vx = Double.parseDouble(vals[0].trim());
            vy = Double.parseDouble(vals[1].trim());
            vz = Double.parseDouble(vals[2].trim());
            res = gd.getNextString();
            vals = res.split(",");
            wx = Double.parseDouble(vals[0].trim());
            wy = Double.parseDouble(vals[1].trim());
            wz = Double.parseDouble(vals[2].trim());

            int val = (int) gd.getNextNumber();
            gauss = gd.getNextBoolean();
            display = gd.getNextChoiceIndex();
            viewer3d = gd.getNextBoolean();

            ObjectCreator3D obj;
            Vector3D V = new Vector3D(vx, vy, vz);
            Vector3D W = new Vector3D(wx, wy, wz);
            if (Math.abs(V.dotProduct(W)) > 0.001) {
                IJ.log("ERROR : vectors should be perpendicular");
            }

            // ellipsoid
            Vector3D a = new Vector3D(vx, vy, vz);
            a.normalize();
            Vector3D b = new Vector3D(wx, wy, wz);
            b.normalize();
            Vector3D c = a.crossProduct(b);
            c.normalize();
            GeomTransform3D transform = new GeomTransform3D(new double[][]{
                    {rx * a.getX(), ry * b.getX(), rz * c.getX(), cx},
                    {rx * a.getY(), ry * b.getY(), rz * c.getY(), cy},
                    {rx * a.getZ(), ry * b.getZ(), rz * c.getZ(), cz},
                    {0, 0, 0, 1}});

            // new stack
            if (display == 0) {
                obj = new ObjectCreator3D(tx, ty, tz);
                obj.setResolution(resXY, resZ, unit);
                obj.createEllipsoidAxesUnit(cx, cy, cz, rx, ry, rz, (float) val, V, W, gauss);
                ImagePlus plusShape = new ImagePlus("Shape3D", obj.getStack());
                cal.pixelWidth = resXY;
                cal.pixelHeight = resXY;
                cal.pixelDepth = resZ;
                plusShape.setCalibration(cal);
                plusShape.setSlice((int) (cz / resZ));
                plusShape.setDisplayRange(0, val);
                plusShape.show();
            } // OVERWRITE
            else if (display == 1) {
                if (stack == null) {
                    IJ.log("No Stack !");
                } else {
                    obj = new ObjectCreator3D(stack);
                    obj.setCalibration(cal);
                    obj.createEllipsoidAxesUnit(cx, cy, cz, rx, ry, rz, (float) val, V, W, gauss);
                    plus.setSlice((int) (cz / resXY));
                    plus.setDisplayRange(0, val);
                    plus.updateAndDraw();
                }
            }
            if (viewer3d) {
                // 3D Viewer
                if (universe == null) {
                    universe = new Image3DUniverse(512, 512);
                    universe.show();
                }
                // new name
                int l = 1;
                while (universe.contains("ellipsoid" + l)) {
                    l++;
                }
                Color foreground = Toolbar.getForegroundColor();
                Content ellipsoid = universe.addTriangleMesh(Object3DSurface.createSphere(transform, 24, 24), new Color3f(foreground.getRed() / 255.0f, foreground.getGreen() / 255.0f, foreground.getBlue() / 255.0f), "ellipsoid" + l);
                ellipsoid.setVisible(true);
            }
        }
    }
}
