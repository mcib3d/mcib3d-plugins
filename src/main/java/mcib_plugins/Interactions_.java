package mcib_plugins;

import ij.IJ;
import ij.ImagePlus;
import ij.WindowManager;
import ij.gui.GenericDialog;
import ij.plugin.PlugIn;
import mcib3d.geom.interactions.InteractionsComputeContours;
import mcib3d.geom.interactions.InteractionsComputeDamLines;
import mcib3d.geom.interactions.InteractionsComputeDilate;
import mcib3d.geom.interactions.InteractionsList;
import mcib3d.image3d.ImageHandler;

// will use class Objects3DPopulationInteractions in next version
public class Interactions_ implements PlugIn {
    boolean methodLINE = true;
    boolean methodDILATE = false;
    boolean methodTOUCH = false;
    // radii for dilation
    float radxy = 1;
    float radz = 1;

    @Override
    public void run(String s) {
        ImagePlus plus = WindowManager.getCurrentImage();
        if (plus == null) {
            IJ.error("Open a labelled image to compute interactions");
            return;
        }
        // process
        ImageHandler image = ImageHandler.wrap(plus);

        if (dialog()) {
            if (methodDILATE) {
                InteractionsList list = new InteractionsComputeDilate(radxy, radxy, radz).compute(image);
                list.getResultsTableOnlyColoc().show("InteractionsDilate");
            }
            if (methodLINE) {
                InteractionsList list = new InteractionsComputeDamLines().compute(image);
                list.getResultsTableOnlyColoc().show("InteractionsLines");
            }
            if (methodTOUCH) {
                InteractionsList list = new InteractionsComputeContours().compute(image);
                list.getResultsTableOnlyColoc().show("InteractionsTouch");
            }
        }
    }

    boolean dialog() {
        GenericDialog dialog = new GenericDialog("Interactions");
        dialog.addMessage("Objects are separated by black lines");
        dialog.addCheckbox("Lines", methodLINE);
        dialog.addMessage("Objects are touching");
        dialog.addCheckbox("Touching", methodTOUCH);
        dialog.addMessage("Objects are separated by empty space");
        dialog.addCheckbox("Dilation", methodDILATE);
        dialog.addNumericField("radius_DilateXY", radxy, 2);
        dialog.addNumericField("radius_DilateZ", radz, 2);
        dialog.showDialog();
        methodLINE = dialog.getNextBoolean();
        methodTOUCH = dialog.getNextBoolean();
        methodDILATE = dialog.getNextBoolean();
        radxy = (float) dialog.getNextNumber();
        radz = (float) dialog.getNextNumber();

        return dialog.wasOKed();
    }
}

