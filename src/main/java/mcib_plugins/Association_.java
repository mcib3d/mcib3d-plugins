package mcib_plugins;

import ij.IJ;
import ij.WindowManager;
import ij.gui.GenericDialog;
import ij.plugin.PlugIn;
import mcib3d.geom2.tracking.TrackingAssociation;
import mcib3d.image3d.ImageHandler;

public class Association_ implements PlugIn {
    private int idxA;
    private int idxB;
    private int costChoice = 0;
    private double minColoc = 0.1;
    private double maxBB = 0;

    @Override
    public void run(String s) {
        if (!dialog()) return;

        ImageHandler img1 = ImageHandler.wrap(WindowManager.getImage(idxA + 1));
        ImageHandler img2 = ImageHandler.wrap(WindowManager.getImage(idxB + 1));

        TrackingAssociation trackingAssociation = new TrackingAssociation(img1, img2, maxBB, minColoc);
        trackingAssociation.getTrackedImage().show(img1.getTitle() + "-association");
    }

    private boolean dialog() {
        int nbima = WindowManager.getImageCount();
        if (nbima < 2) {
            IJ.error("Needs at least two labelled images");
            return false;
        }
        idxA = 0;
        idxB = 1;
        String[] namesA = new String[nbima];
        String[] namesB = new String[nbima];
        for (int i = 0; i < nbima; i++) {
            namesA[i] = WindowManager.getImage(i + 1).getShortTitle();
            namesB[i] = WindowManager.getImage(i + 1).getShortTitle();
        }
        String[] costChoices = new String[]{"Colocalisation", "Distance Border-Border"};
        GenericDialog dia = new GenericDialog("Association");
        dia.addMessage("Images to associate");
        dia.addChoice("Image_A", namesA, namesA[idxA]);
        dia.addChoice("Image_B", namesB, namesB[idxB]);
        dia.addMessage("Method for association");
        dia.addChoice("Method", costChoices, costChoices[costChoice]);
        dia.addNumericField("Min coloc (% object in A)", minColoc * 100.0, 5);
        dia.addNumericField("Max dist BB (pixel unit)", maxBB, 3);
        dia.showDialog();
        if (dia.wasOKed()) {
            idxA = dia.getNextChoiceIndex();
            idxB = dia.getNextChoiceIndex();
            costChoice = dia.getNextChoiceIndex();
            minColoc = dia.getNextNumber() / 100.0;
            maxBB = dia.getNextNumber();
        }

        return dia.wasOKed();
    }
}
