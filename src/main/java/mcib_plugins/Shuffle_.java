package mcib_plugins;

import ij.IJ;
import ij.WindowManager;
import ij.gui.GenericDialog;
import ij.plugin.PlugIn;
import mcib3d.image3d.ImageHandler;
import mcib3d.image3d.processing.ImageShuffler;

public class Shuffle_ implements PlugIn {
    private int imaMask, imaSeg;

    @Override
    public void run(String s) {
        if (Dialogue()) {
            ImageHandler seg = ImageHandler.wrap(WindowManager.getImage(imaSeg));
            ImageShuffler shuffler = new ImageShuffler(seg);
            if ((imaMask > 0) && (imaMask != imaSeg + 1))
                shuffler.setMask(ImageHandler.wrap(WindowManager.getImage(imaMask)));
            ImageHandler shuffle = shuffler.shuffle();
            if (shuffle != null) {
                shuffle.show();
                shuffle.setTitle(seg.getTitle() + "_shuffle");
            }
        }
    }

    private boolean Dialogue() {
        int nbima = WindowManager.getImageCount();
        if (nbima < 1) {
            IJ.showMessage("No image opened !");
            return false;
        }
        String[] namesSeg = new String[nbima];
        String[] namesMask = new String[nbima + 1];

        namesMask[0] = "None";
        for (int i = 0; i < nbima; i++) {
            namesSeg[i] = WindowManager.getImage(i + 1).getShortTitle();
            namesMask[i + 1] = WindowManager.getImage(i + 1).getShortTitle();
        }
        imaSeg = 0;
        imaMask = 0;

        GenericDialog dia = new GenericDialog("Statistical measure");
        dia.addChoice("Objects", namesSeg, namesSeg[imaSeg]);
        dia.addChoice("Mask", namesMask, namesMask[imaMask]);
        dia.showDialog();
        imaSeg = dia.getNextChoiceIndex() + 1;
        imaMask = dia.getNextChoiceIndex();

        return dia.wasOKed();
    }
}
